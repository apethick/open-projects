package curtin.resistivity.resistomatic;
public class Point {

    public double x;
    public double y;
    public double z;
    public double w;
    
    public int n;
    
    public Point(double x) {
     this.x = x;
     this.y = 0;
     this.z = 0;
     this.w = 0;
     
     n = 1;
    }
    
    public Point(double x, double y, double z, double w) {
     this.x = x;
     this.y = y;
     this.z = z;
     this.w = w;
     n=4;
     
    }
    
    public Point(double x, double y, double z) {
     this.x = x;
     this.y = y;
     this.z = z;    
     this.w = 0;
     n=3;
    }
    
    public Point (double x, double y) {
     this.x = x;
     this.y = y;
     this.z = 0;    
     this.w = 0;
     n=2;
    }
    
    public double radius(Point p2) {
    	
        double d = 0;
        switch(n) {
         case 1 : 
           d= pow(pow(x-p2.x,2),0.5); break;
         case 2 :
         d= (pow(pow(x-p2.x,2)+pow(y - p2.y,2),0.5)); break;
         case 3 :
         d= (pow(pow(x-p2.x,2)+pow(y - p2.y,2)+pow(z-p2.z,2),0.5)); break;
         case 4 :
         d= (pow(pow(x-p2.x,2)+pow(y - p2.y,2)+pow(z-p2.z,2) + +pow(w-p2.w,2),0.5)); break;
         default :
         d= 0;
        }
        return d;
        
    }
    
    public String StringShort(){
     String s = "";   
     if (n==1) {
          s = s + "(" + x + ")";   
        }
     if (n == 2) {
           s = s+ "(" + x + " , "  + y + ")";
        }
        if (n == 3) {
           s = s+ "(" + x + " , "  + y + " , " + z + ")";
        }
        if (n == 4) {
           s = s + "x = " + x + "\n        y = "  + y  + "\n        z = " + z;
           s = s + "\n        w = " + w;
        }
        
        s = s + "\n";
        return s;
    }
    
    public String toString() {
       String s = "Point : ";
       if (n == 2) {
           s = s+ "x = " + x + "\n        y = "  + y;
        }
        if (n == 3) {
           s = s +"x = " + x + "\n        y = "  + y +  "\n        z = " + z;
        }
        if (n == 4) {
           s = s + "x = " + x + "\n        y = "  + y  + "\n        z = " + z;
           s = s + "\n        w = " + w;
        }
        if (n==1) {
          s = s + "x = " + x;   
        }
        s = s + "\n";
        return s;
    }
    
    public boolean is1D () {
     return (n == 1);   
    }
    
    private double pow(double a, double b) {
     return java.lang.Math.pow(a,b);
    }
    
        
    }