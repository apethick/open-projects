package curtin.resistivity.resistomatic;
import java.util.Iterator;

public class Temp  {
    public Temp() {
        
    }
    
    public static Resistivity array(){
    
     Resistivity r = new Resistivity(new Point(3),new Point(2));
     
     QueueLink q = new QueueLink();
    
     for (double i = 9 ; i < 17 ; i += 1) {
         q.enqueue(new wrapper(new Point(i)));
        }
     for (double i = 9 ; i < 17 ; i += 1) {
         q.enqueue(new wrapper(new Point(i), new Point(i+1)));
        }
        
        r.potentials = q;
     return r;
    }
    
    public static double [] range(wrapper wrap, Resistivity r) {
        
   //  Resistivity r = Temp.array();
     
    
     double maxC1C2 = 0;
     double minC1C2 = 0;
     
     if (r.oneElectrode) {
          double C1 = r.C1.x;
         maxC1C2 = C1;
         minC1C2 = maxC1C2;
        }
     else {
         double C1 = r.C1.x;
         double C2 = r.C2.x;
         maxC1C2 = java.lang.Math.max(C1,C2);
         minC1C2 = java.lang.Math.min(C1,C2);
        }
        
     wrapper w = wrap;  
      double maxP1P2 = 0;
      double minP1P2 = 0;
      
      if (w.n == 1) {
          maxP1P2 = w.p1.x;
          minP1P2 = w.p1.x;
        }
      else {
          maxP1P2 = java.lang.Math.max(w.p1.x, w.p2.x);
          minP1P2 = java.lang.Math.min(w.p1.x, w.p2.x);
        }
        
       double minimumValue = java.lang.Math.min(minP1P2, minC1C2);
       double maximumValue = java.lang.Math.max(maxP1P2, maxC1C2);
      int TenPercent = (int) ((maximumValue - minimumValue)/10);
        if (TenPercent < 2) {
            TenPercent++;
        }
     double min = minimumValue - TenPercent;
     double max = maximumValue + TenPercent;
     
     double [] a = {min,max};
     return a;
     
     
    }
    
    }