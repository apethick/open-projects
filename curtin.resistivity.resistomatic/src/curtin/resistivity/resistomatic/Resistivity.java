package curtin.resistivity.resistomatic;

public class Resistivity{
    
    public QueueLink potentials;
    
    double PI = java.lang.Math.PI;
    Point C1;
    Point C2;   
    
    double I;
    double V;
    boolean hasVI = false;
    wrapper w;
   
    boolean oneElectrode;
    
    public Resistivity (Point C1, Point C2, wrapper w) {
     this.C1 = C1;
     this.C2 = C2;
     if (C1.equals(null) || C2.equals(null)) {
         oneElectrode = true;
        }
     else oneElectrode = false;
     this.w = w;
    }
    
    
    
    public Resistivity (Point C1, Point C2) {
     this.C1 = C1;
     this.C2 = C2;
     if (C1.equals(null) || C2.equals(null)) {
         oneElectrode = true;
        }
     else oneElectrode = false;
    }

    
    public Resistivity (Point C1) {
        this.C1 = C1;
       oneElectrode = true;
    }
    
    public void setVoltCurr(double voltage, double current) {
     V = voltage;
     I = current;
     hasVI = true;
    }
    public void noVoltCurr() {
     hasVI = false;   
    }
    
    public Resistivity (double c1x, double c1y, double c2x, double c2y) {
        C1 = new Point(c1x,c1y);
        C2 = new Point(c2x,c2y);
        oneElectrode = false;
    }
    public Resistivity (double c1x, double c1y) {
        C1 = new Point(c1x,c1y);
        oneElectrode = true;
    }
    
    public double kValue(Point p1) {
        double k = 0;
        if (oneElectrode) {
        double r1 = pow(pow((C1.x - p1.x),2.0) + pow((C1.y - p1.y),2),0.5);
        k = 2*PI*(1/((1/r1)));
        }
        else {
        double r1 = pow(pow((C1.x - p1.x),2.0) + pow((C1.y - p1.y),2),0.5);
        double r3 = pow(pow((C2.x - p1.x),2.0) + pow((C2.y - p1.y),2),0.5);
        k = 2*PI*(1/((1/r1)-(1/r3)));
       
        }
        return k;
    }
    

    
     public double kValue(Point p1, Point p2) {
         double k = 0;
        if (oneElectrode) {
            double r1 = pow(pow((C1.x - p1.x),2.0) + pow((C1.y - p1.y),2),0.5);
            double r2 = pow(pow((C1.x - p2.x),2.0) + pow((C1.y - p2.y),2),0.5);
            k = 2*PI*(1/((1/r1)-(1/r2)));
        }
        else {
            double r1 = pow(pow((C1.x - p1.x),2.0) + pow((C1.y - p1.y),2),0.5);
            double r2 = pow(pow((C1.x - p2.x),2.0) + pow((C1.y - p2.y),2),0.5);
            double r3 = pow(pow((C2.x - p1.x),2.0) + pow((C2.y - p1.y),2),0.5);
            double r4 = pow(pow((C2.x - p2.x),2.0) + pow((C2.y - p2.y),2),0.5);
            k = 2*PI*(1/((1/r1)-(1/r2)-(1/r3) + (1/r4)));
        }
        return k;
    }
    
    public String rockType(double ard) {
        double ar = java.lang.Math.abs(ard);
     String s = "Possible Rock Types :^";
     if (ar < 50000 && ar > 800) {
         s += "Fresh Granite^";
        }
      if(ar < 90 && ar > 1) {
         s += "Weathered Granite^"; 
        }
      if (ar > 65) {
         s += "Basalt^"; 
        }
      if (ar > 1 && ar < 100) {
         s += "Jointed Fractured and Top flow Basalt^"; 
        }
      if (ar < 0.55 && ar > 0.35) {
         s += "Sea Water^"; 
        }
      if (ar > 1 && ar < 1000) {
         s += "Argillite^"; 
        }
      
      if (ar < 0.9) {
         s += "Brine/Geothermal Fluid^"; 
        }
      if (ar > 5 && ar < 850) {
         s += "Sandstone^"; 
        }
      if (ar > 30 && ar < 5000) {
         s +=  "Limestone^";
        }
      if (ar > 1000) {
         s += "Quartzite^"; 
        }
      if ( ar > 1 && ar < 200) {
         s += "Alluvium^"; 
        }
      if (ar > 0.1 && ar < 8) {
         s += "Graphitic Schist^"; 
        }
      if (ar < 0.03) {
         s += "Massive Sulphides^"; 
        }
      if (ar < 1 && ar > 0.1) {
         s += "Pure Melt^"; 
        }
      if (ar < 40 && ar > 0.6) {
         s += "Partially Molten Rocks^"; 
        }
      if (ar > 1 && ar < 100) {
         s += "Clay^"; 
        }
      if (ar > 100 && ar < 8000) {
         s += "Gravel^"; 
        }
     
     return s;
    }
    
    public double MaxCurrentDensity() {
      return ((C1.radius(C2))/pow(2,0.5));
    }
    
    public void changeC1(Point p) {
       C1 = p;   
    }
    
    public void changeC2(Point p) {
       C2 = p;   
    }
    
    public void printInfo() {
     System.out.println("\n");
     System.out.println("------------ Start of Resistivity Array --------------");
     System.out.println("Current Electrode array\n--------------------------\n");
     if (oneElectrode) {
     
     System.out.println("1 Electrode Array");
     System.out.println("Current Electrode C1 : x = " + C1.x);
     System.out.println("                       y = " + C1.y);
     
    }
    else {
     System.out.println("2 Electrode Array");
     System.out.println("Current Electrode C1 : x = " + C1.x);
     System.out.println("                       y = " + C1.y);
     System.out.println("Current Electrode C2 : x = " + C2.x);
     System.out.println("                       y = " + C2.y); 
     System.out.println("\nDistance between Electrodes = " + C1.radius(C2));
     System.out.println("Maximum Current Density at = " + MaxCurrentDensity() + "\n");
     
    }
    
    
    
     
        System.out.println("------------- End of Resistivity Array ---------------");
        System.out.println();
        System.out.println();
    }
    
    public String ArrayShort () {
     String s = "";
     if (oneElectrode) {
         s = s + "C1 : " + C1.StringShort();
        }
      else {
          s = s + "C1 : " + C1.StringShort();
          s = s + "C2 : " + C2.StringShort();
        }
     return s;
    }
    
    public void printEq() {
        System.out.println("---- START OF EQUATIONS ----");
     if (oneElectrode) {
         System.out.println("    ARI    1       1     ");
         System.out.println("V = --- ( ---  -  --- )  ");
         System.out.println("    2pi    r1      r2    ");
         
         System.out.println("\nApparent resistivity =");
         System.out.println("      V    ");
         System.out.println("AR = --- k ");
         System.out.println("      I    ");
         
         System.out.println();
         System.out.println("           1       1   -1 ");
         System.out.println("k = 2*pi*(---  -  --- )   ");
         System.out.println("           r1      r2     ");
        }
     else {
       
         System.out.println("     ARI    1       1     1     1   ");
         System.out.println("V =  ---  (---  -  --- - --- + --- )");
         System.out.println("     2pi    r1      r2    r3    r4  ");
         System.out.println("\nApparent resistivity =");
         System.out.println("      V    ");
         System.out.println("AR = --- k ");
         System.out.println("      I    ");
         System.out.println();
         System.out.println("           1       1     1     1   -1 ");
         System.out.println("k = 2*pi*(---  -  --- - --- + --- )   ");
         System.out.println("           r1      r2    r3    r4      ");
        }
        System.out.println("----- END OF EQUATIONS -----");
    }
    
   
    
    public double r1(Point p1) {        
        double r = pow(pow((C1.x - p1.x),2.0) + pow((C1.y - p1.y),2),0.5);
        return r;
    }
    public double r2(Point p2) {
        double r = pow(pow((C1.x - p2.x),2.0) + pow((C1.y - p2.y),2),0.5);
        return r;
    }
    
    public double r3(Point p1) {
        double r = -1;
        if (oneElectrode) {
            r = -1;
        }
        
        else {
        r = pow(pow((C2.x - p1.x),2.0) + pow((C2.y - p1.y),2),0.5);
     }
     return r;
    }
    
    public double r4(Point p2) {
        double r = -1;
        if (oneElectrode) {
        r = -1;
    }
    else {
     r = pow(pow((C2.x - p2.x),2.0) + pow((C2.y - p2.y),2),0.5);   
    }
    return r;
      
    }
    
    public double AR (double k, double Current, double Voltage) {
     double ar = 0;
     ar = (Voltage/Current)*k;
     return ar;
        
        
    }
    
   
    
    
    
    
    private double pow(double a, double b) {
     return java.lang.Math.pow(a,b);
    }
}
