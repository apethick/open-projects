package curtin.resistivity.resistomatic;
public class wrapper{
    
    public Point p1;
    public Point p2;
    int n;
    
    public wrapper (Point p1) {
        this.p1 = p1;
        n = 1;
       }
       
   public wrapper (Point p1, double voltage) {
        this.p1 = p1;
        n = 1;
       }
       
    public wrapper (Point p1, Point p2) {
     this.p1 = p1;
     this.p2 = p2;
     n = 2;
    }
    
    public wrapper (Point p1, Point p2, double voltage) {
     this.p1 = p1;
     this.p2 = p2;
     n = 2;
    }
    
    
    
    public String toString() {
        String s = "";
        if (n == 1) {
            if (p1.n == 1) {
           s = s + "P1 : (" + p1.x + ")";
            }
            else {
          s = s + "P1 : (" + p1.x + " , " + p1.y + ")";
        }
        }
        else {
            if (p1.n == 1) {
           s = s + "P1 : (" + p1.x + ")";
           s = s + "\nP2 : (" + p2.x + ")";
            }
            else {
          s = s + "P1 : (" + p1.x + " , " + p1.y + ")";
          s = s + "\nP2 : (" + p2.x + " , " + p2.y + ")";
        }
        }
          
        return s;
    }
}
