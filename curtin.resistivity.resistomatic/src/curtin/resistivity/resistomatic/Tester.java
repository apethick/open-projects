package curtin.resistivity.resistomatic;
import java.util.Iterator;
public class Tester {

    public static void main() {
        PointTests();
        QueueTests();
        wrapperTests();
        ResistivityTests();
        AnalyserTests();
    }
    
    public static void ResistivityTests() {
        
        System.out.println("\n\n\n ================= RESISTIVITY TESTS =============================");
        
        System.out.println("- Creating new Resistivity Array, 1 Point (Point)");
        Resistivity r1 = new Resistivity(new Point(0.1,5.1));
        System.out.println("- Created Resistivity Array, 1 Point (Point)");
        r1.printInfo();
 
        r1.printEq();
        System.out.println("\n Testing for values of k, P1(0.9,3.4)");
        System.out.println("k = " + r1.kValue(new Point(0.9,3.4)));
        System.out.println("\n Testing for values of k, P1(0.9,3.4)  P2(7.4,12.7)");
        System.out.println("k = " + r1.kValue(new Point(0.9,3.4), new Point(7.4,12.7)));
        System.out.println("\n\n");
        
        System.out.println("- Creating new Resistivity Array, 1 Point (Double)");
        Resistivity r2 = new Resistivity(0.1,5.1);
        System.out.println("- Created Resistivity Array, 1 Point (Double)");
        r2.printInfo();
  
        r2.printEq();
        System.out.println("\n Testing for values of k, P1(0.9,3.4)");
        System.out.println("k = " + r2.kValue(new Point(0.9,3.4)));
        System.out.println("\n Testing for values of k, P1(0.9,3.4)  P2(7.4,12.7)");
        System.out.println("k = " + r2.kValue(new Point(0.9,3.4), new Point(7.4,12.7)));
        System.out.println("\n\n");
        
        System.out.println("- Creating new Resistivity Array, 2 Point (Point)");
        Resistivity r3 = new Resistivity(new Point(0.1,5.1), new Point(20.3,19.1));
        System.out.println("- Created Resistivity Array, 2 Point (Point)");
        r3.printInfo();
 
        r3.printEq();
         System.out.println("\n Testing for values of k, P1(0.9,3.4)");
        System.out.println("k = " + r3.kValue(new Point(0.9,3.4)));
        System.out.println("\n Testing for values of k, P1(0.9,3.4)  P2(7.4,12.7)");
        System.out.println("k = " + r3.kValue(new Point(0.9,3.4), new Point(7.4,12.7)));
        System.out.println("\n\n");
        
        System.out.println("- Creating new Resistivity Array, 2 Point (Double)");
        Resistivity r4 = new Resistivity(0.1,5.1,20.3,19.1);
        System.out.println("- Created Resistivity Array, 2 Point (Double)");
        r4.printInfo();
    
        r4.printEq();
         System.out.println("\n Testing for values of k, P1(0.9,3.4)");
        System.out.println("k = " + r4.kValue(new Point(0.9,3.4)));
        System.out.println("\n Testing for values of k, P1(0.9,3.4)  P2(7.4,12.7)");
        System.out.println("k = " + r4.kValue(new Point(0.9,3.4), new Point(7.4,12.7)));
        System.out.println("\n\n");
        
        
    }
    
    public static void PointTests() {
        System.out.println("\n\n\n ====================== POINT TESTS ================================");
        System.out.println("Creating a new (x) Point 1");
        Point p = new Point(0.51);
        System.out.println("new Point Created");
        System.out.println("Contents of Point : ");
        System.out.println(p.toString());
        System.out.println("\nCreating a new (x) Point 2");
        Point p2 = new Point(0.51);
        System.out.println("new Point Created");
        System.out.println("Contents of Point : ");
        System.out.println(p2.toString());
        System.out.println("\n radius from P1 to P2 (should be 0)");
        System.out.println("radius = " + p.radius(p2));
        System.out.println("\n radius from P2 to P1 (should be 0)");
        System.out.println("radius = " + p2.radius(p));
        System.out.println("Changing position of P2 to (42)");
        p2.x = 42;
        System.out.println("Contents of Point : ");
        System.out.println(p2.toString());
        System.out.println("\n radius from P1 to P2 (should be 41.49)");
        System.out.println("radius = " + p.radius(p2));
        System.out.println("\n radius from P2 to P1 (should be 41.49)");
        System.out.println("radius = " + p2.radius(p));
        System.out.println();       
        
        System.out.println("Creating a new (x,y) Point 1");
        p = new Point(0.51,0.62);
        System.out.println("new Point Created");
        System.out.println("Contents of Point : ");
        System.out.println(p.toString());
        System.out.println("\nCreating a new (x,y) Point 2");
        p2 = new Point(0.51,0.62);
        System.out.println("new Point Created");
        System.out.println("Contents of Point : ");
        System.out.println(p2.toString());
        System.out.println("\n radius from P1 to P2 (should be 0)");
        System.out.println("radius = " + p.radius(p2));
        System.out.println("\n radius from P2 to P1 (should be 0)");
        System.out.println("radius = " + p2.radius(p));
        System.out.println("Changing position of P2 to (42,32)");
        p2.x = 42;
        p2.y = 32;
        System.out.println("Contents of Point : ");
        System.out.println(p2.toString());
        System.out.println("\n radius from P1 to P2 (should be 52.02)");
        System.out.println("radius = " + p.radius(p2));
        System.out.println("\n radius from P2 to P1 (should be 52.02)");
        System.out.println("radius = " + p2.radius(p));
        
        
        System.out.println("\nCreating a new (x,y,z) Point");
        p = new Point(0.51,0.62,-0.25);
        System.out.println("new Point Created");
        System.out.println("Contents of Point : ");
        System.out.println(p.toString());
        System.out.println("\nCreating a new (x,y,z,w) Point");
        p = new Point(0.51,0.62,-0.25,-15466465.12612651);
        System.out.println("new Point Created");
        System.out.println("Contents of Point : ");
        System.out.println(p.toString());
    }
    
    public static void QueueTests () {
        System.out.println("\n\n\n ====================== QUEUE TESTS ================================");
        System.out.println("Creating new queue");
        QueueLink q = new QueueLink();
        System.out.println("Checking if Queue is empty (result should be true)");
        System.out.println("is Queue Empty = " + q.isEmpty());
        System.out.println("Enquing Points to Queue");
        Point p = new Point(0.1,0.2);
        q.enqueue(p);
        p = new Point(0.3,0.4);
        q.enqueue(p);
        p = new Point(0.5,0.6);
        q.enqueue(p);
        p = new Point(0.7,0.8);
        q.enqueue(p);
        p = new Point(0.9,1.0);
        q.enqueue(p);

        System.out.println("Finished Enqueuing Points");
        System.out.println("Queue Contents : \n" + q.toString());
        System.out.println("Checking if Queue is empty (result should be false)");
        System.out.println("is Queue Empty = " + q.isEmpty());
        System.out.println("\n--- Examining Queue using queue Iterator ---");
        System.out.println("Creating Queue Iterator");
        Iterator It = q.iterator();
        while (It.hasNext()) {
         Object o = It.next();
         System.out.println("examining : ");
         System.out.println(o.toString());
        }
        
        
        System.out.println("\n--- Dequeuing Queue ---");
        while (!q.isEmpty()) {
         Object o = q.dequeue();
         System.out.println("Dequeuing, returning : \n" + o.toString());
        }
        
        
        
    }
    
    public static void wrapperTests() {
       System.out.println("\n\n\n ====================== WRAPPER TESTS ================================");
       QueueLink q = new QueueLink();
       System.out.println("Created Queue to store Point. Importing Single Point wrappers : ");
       for (double i = 0.1 ; i < 2 ; i = i + 0.2) {
           Point p1 = new Point(i, i+0.1);
            wrapper w = new wrapper(p1);

         System.out.println("Inserting wrapper : \n" + w.toString());
           
        }
        System.out.println("Created Queue to store Point. Importing Double Point wrappers : ");
       for (double i = 1 ; i < 20 ; i = i+4) {
           Point p1 = new Point(i, i+1);
           Point p2 = new Point(i+2, i+3);
            wrapper w = new wrapper(p1,p2);
            
         System.out.println("Inserting wrapper : \n" + w.toString());
           
        }
        
    }
    
    public static void AnalyserTests() {
        System.out.println("\n\n\n ====================== ANALYSER TESTS ================================");
        System.out.println("Creating new Resistivity Arrays");
        Resistivity r1 = new Resistivity(new Point(5));
        System.out.println("Resistivity array 1");
        r1.printInfo();
        Resistivity r2 = new Resistivity(new Point(5,7));
        System.out.println("Resistivity array 2");
        r2.printInfo();
        Resistivity r3 = new Resistivity(new Point(5), new Point(14));
        System.out.println("Resistivity array 3");
        r3.printInfo();
        Resistivity r4 = new Resistivity(new Point(5,7), new Point(14,9));
        System.out.println("Resistivity array 4");
        r4.printInfo();
        System.out.println("Creating new queue of points for k values");
        QueueLink q= new QueueLink();
       for (int i = 1 ; i < 5 ; i++) {
            Point p1 = new Point(i);
            wrapper w = new wrapper(p1);
            q.enqueue(w);
         System.out.println("Inserting wrapper : \n" + w.toString());
        }
       for (double i = 0.1 ; i < 2 ; i = i + 0.2) {
            Point p1 = new Point(i, i+0.1);
            wrapper w = new wrapper(p1);
            q.enqueue(w);
         System.out.println("Inserting wrapper : \n" + w.toString());
        }
        for (double i = 1 ; i < 20 ; i = i+4) {
           Point p1 = new Point(i, i+1);
           Point p2 = new Point(i+2, i+3);
            wrapper w = new wrapper(p1,p2);
            q.enqueue(w);
         System.out.println("Inserting wrapper : \n" + w.toString());
           
        }
        Iterator it = q.iterator();
        System.out.println("Positions of potential Electrodes");
        int index = 1;
        while (it.hasNext()) {
         wrapper w = (wrapper) it.next();  
         System.out.println("Electrode array " + index);
         System.out.println(w.toString());
         index++;
        }
        System.out.println("======= Resistivity Array 1 ========");
        Analyser.kValues(r1, q);
        System.out.println("======= Resistivity Array 2 ========");
        Analyser.kValues(r2, q);
        System.out.println("======= Resistivity Array 3 ========");        
        Analyser.kValues(r3, q);
        System.out.println("======= Resistivity Array 4 ========");        
        Analyser.kValues(r4, q);
    }

}