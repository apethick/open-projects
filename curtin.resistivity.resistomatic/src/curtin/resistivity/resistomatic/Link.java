package curtin.resistivity.resistomatic;
 

public class Link {
    
    public Object item;
    public Link successor;
    public Link predecessor;
    
    public Link (Object item, Link successor) {
     this.item = item;
     this.successor = successor;
    }
    public Link (Object item, Link successor, Link predcessor) {
     this.item = item;
     this.successor = successor;
     this.predecessor = predecessor;
    }

}