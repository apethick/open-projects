package curtin.resistivity.resistomatic;

import java.applet.Applet;
import java.awt.*;
import javax.swing.*;

public class Resistomatic extends JFrame {
	
	
	public Resistomatic() {
	 
	
	 
		Applet applet = new Diagram();
		applet.init();
		applet.start();
		applet.setSize(1000, 750);
	    
	    this.setTitle("RESISToMatic - Written By Andrew Pethick");
	    this.getContentPane().add(applet);
	    this.pack();
	    this.setSize(1000,750);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.setResizable(false);
	    this.show();
	}
	
	public static void main(String[] args) {
		new Resistomatic();
	}
	
}