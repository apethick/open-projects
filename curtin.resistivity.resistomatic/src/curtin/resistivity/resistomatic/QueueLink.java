package curtin.resistivity.resistomatic;


import java.util.Iterator;
import java.util.function.Consumer;

public class QueueLink{
    
    private Link first;
    private Link last;

    public QueueLink () {
        first = null;
        last = null;
    }

    public boolean isEmpty() {
        return (first == null);
    }
    
    public void enqueue (Object o) {
        if (isEmpty()) {
          
         first = new Link(o,null, last);
         last = first;
        }
        else {
         last.successor = new Link(o,null,last);
         last = last.successor;
         last.successor = first;
        }
    }
    
    public Object dequeue() {
     if (!isEmpty()) {
         Object o = examine();
         first = first.successor;
         if (isEmpty()) last=null;
         return o;
        }
     else throw new java.lang.NullPointerException("Cannot Dequeue empty queue");
        
    }
    
    
    
    public Object examine(){
    if (!isEmpty()) {
        return (first.item);
    }
    else throw new java.lang.NullPointerException("Cannot Eamine Empty Queue");
    }
    
    public String toString() {
        Iterator it = iterator();
        String st = "";
        if (isEmpty()) return "Queue is Empty";
        while (it.hasNext()) {
         st += it.next().toString();  
         st += " ";
        }
        return st;
    }
    public Iterator iterator() {
     return new QueueIterator();   
    }
    
    
    public class QueueIterator implements Iterator {
        
        Link f;
        Link l;
        
        QueueIterator () {
          f = first;
          l = last;
        }
        
        public boolean hasNext() {
         return (f != null);   
        }
        
        public Object previous() {
            
          //  f = f.predecessor;
            
            Link current = f;
            if (current != first) {
            Link Temp = first;
            while (Temp.successor != current) {
             Temp = Temp.successor;   
            }
            f = Temp;
        }
           else {
            Link Temp = first;
            while (Temp != last) {
             Temp = Temp.successor;   
            }
            f = Temp;
           
            }

             Object o = f.item;
           
             return o;
             
             
        }
        
        public Object next() {
         if (!hasNext()) {
             f = first;
             l = last;
             Object o = f.item;
             f = f.successor;
             return o;
            }
          else {
              Object o = f.item;
              f = f.successor;
             return o; 
            }
        }
        public void remove() {
            
        }

		@Override
		public void forEachRemaining(Consumer action) {
			// TODO Auto-generated method stub
			
		}
    }
    
    

}