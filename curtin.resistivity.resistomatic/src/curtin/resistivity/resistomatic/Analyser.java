package curtin.resistivity.resistomatic;
import java.util.Iterator;

public class Analyser {
    
    public static void kValues(Resistivity r, QueueLink q) {
        Iterator it = q.iterator();
        while (it.hasNext()) {
           wrapper w = (wrapper) it.next();
           Point p1 = w.p1;
           Point p2 = w.p2;
           int n = w.n;
           
           if (n==1) {
             System.out.print(r.ArrayShort());
             System.out.println(w.toString());
             System.out.println("k = " + r.kValue(p1));
             System.out.println();
            }
            if (n==2) {
             System.out.print(r.ArrayShort());
             System.out.println(w.toString());
             System.out.println("k = " + r.kValue(p1,p2));   
             System.out.println();
             
            }
           
         }
            
       }
 }

    