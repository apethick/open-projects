package curtin.resistivity.resistomatic;
import java.awt.*;
import java.applet.*;
import java.util.Iterator;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.lang.Double;
import java.net.URL;

import javax.imageio.ImageIO;

public class Diagram extends java.applet.Applet implements ActionListener{
  // ===================== START OF INSTANCE VARIABLES  ===================== 
  Color Blue = new Color(0,104,150);
  
   Resistivity r;
   Iterator it;
   Iterator rit ;
   QueueLink q;
   wrapper w;
   double min; 
   double max; 
   double k = 0;
   File data =  new File("DATA.txt");
//   QueueLink arrays = Parser.parse();
    private javax.swing.JButton Next;
    private javax.swing.JButton Previous;
    boolean hasNoArray = true;
    boolean lastNext = true;
    boolean menuBoolean = true;
    boolean wennerBoolean = false;
    boolean slumbergerBoolean = false;
    boolean hasApparentResistivity = false;
    boolean start = true;
    TextField Cur1 ;
    TextField Cur2 ;
    TextField Pot1 ;
    TextField Pot2 ;
    TextField Volt;
    TextField Curr;
    TextField wennerText = new TextField(2);
    TextField slumbText = new TextField(2);
    
    BufferedImage down = null;
    BufferedImage leftInfinite = null;
    BufferedImage rightInfinite = null;
    BufferedImage formula = null;
    BufferedImage resistomatic = null;
    BufferedImage pot = null;
    BufferedImage logo = null;
    BufferedImage wennerImag = null;
    BufferedImage slumbergerImag = null;
    
      Panel p = new Panel();
              Label c1, c2, p1, p2, Vo, Cu;

        Label space = new Label("   ");
        Label wennern = new Label ("a");
        Label slumbn  = new Label ("n");

          Button next = new Button("  NEXT  ");
          Button refresh = new Button("Refresh");
          Button previous = new Button("PREVIOUS");
          Button clear = new Button("Clear");
          Button menu = new Button("MENU");
          Button wenner = new Button("Wenner");
          Button slumberger = new Button("Slumberger");
          Button custom = new Button("Custom");
//          Button fromFile = new Button("From File");
          Button slumbergerRefresh = new Button("Refresh");
          Button wennerRefresh = new Button("Refresh");
          
    // =====================  END OF INSTANCE VARIABLES===================== 
   
   
   
   
    public void init() {
    	this.setSize(1000, 700);
    	
     r = new Resistivity (new Point(1),new Point(2));
     w = new wrapper(new Point(3),new Point(4));
     try {
    	System.out.println();
 		formula = ImageIO.read(this.getClass().getResource("formula.gif"));
 		resistomatic = ImageIO.read(this.getClass().getResource("resistomatic.jpg"));
 		down = ImageIO.read(this.getClass().getResource("down.gif"));
 		pot =ImageIO.read(this.getClass().getResource("pot.gif"));
 	    logo = ImageIO.read(this.getClass().getResource("logo.gif"));
 	    rightInfinite = ImageIO.read(this.getClass().getResource("rightInfinite.gif"));
 	    leftInfinite =ImageIO.read(this.getClass().getResource("leftInfinite.gif"));
 	    wennerImag = ImageIO.read(this.getClass().getResource("wenner.gif"));
 	    slumbergerImag = ImageIO.read(this.getClass().getResource("slumberger.gif"));
 	} catch (IOException e) {
 		// TODO Auto-generated catch block
 		e.printStackTrace();
 	}
     
//       rit = arrays.iterator();
//       r = (Resistivity) rit.next();
//       w = r.w;
      
     /*  rit = arrays.iterator();
       rit.next();
       r = (Resistivity) rit.next();
       w = r.w;
       //r = Temp.array();
       it = r.potentials.iterator();
      q = r.potentials;  
        it.next();
      //w = (wrapper) q.examine();*/
      
      
      setLayout(new BorderLayout());
       
        c1 = new Label("C1");
        c2 = new Label("C2");
        p1 = new Label("P1");
        p2 = new Label("P2");
        Vo = new Label("V");
        Cu = new Label("I");
      Cur1 = new TextField(2);
      Cur2 = new TextField(2);
      Pot1 = new TextField(2);
      Pot2 = new TextField(2);
      Volt = new TextField(2);
      Curr = new TextField(2);
         
      c1.setFont(new Font("Courier", Font.BOLD, 14));
      c2.setFont(new Font("Courier", Font.BOLD, 14));
      p1.setFont(new Font("Courier", Font.BOLD, 14));
      p2.setFont(new Font("Courier", Font.BOLD, 14));
      Vo.setFont(new Font("Courier", Font.BOLD, 14));
      Cu.setFont(new Font("Courier", Font.BOLD, 14));
      wennern.setFont(new Font("Courier", Font.BOLD, 18));
      slumbn.setFont(new Font("Courier", Font.BOLD, 18));    
      clear.setBackground(Color.BLACK);
      clear.setForeground(Color.WHITE);
      clear.setFont(new Font("Courier", Font.BOLD, 20));
      slumbergerRefresh.setBackground(Color.BLACK);
      slumbergerRefresh.setForeground(Color.WHITE);
      slumbergerRefresh.setFont(new Font("Courier", Font.BOLD, 20));
      wennerRefresh.setBackground(Color.BLACK);
      wennerRefresh.setForeground(Color.WHITE);
      wennerRefresh.setFont(new Font("Courier", Font.BOLD, 20));
      
       next.setBackground(Color.WHITE);
       next.setForeground(Blue);
       next.setFont(new Font("Courier", Font.BOLD, 20));
       previous.setBackground(Color.WHITE);
       previous.setForeground(Blue);
       previous.setFont(new Font("Courier", Font.BOLD, 20));
       refresh.setBackground(Color.BLACK);
       refresh.setForeground(Color.WHITE);
       refresh.setFont(new Font("Courier", Font.BOLD, 20));
       p.setBackground(Color.WHITE);
       p.add(menu);
       menu.setBackground(Color.BLACK);
       menu.setForeground(Color.WHITE);
       menu.setFont(new Font("Courier", Font.BOLD, 20));
       wenner.setBackground(Color.BLACK);
       wenner.setForeground(Color.WHITE);
       wenner.setFont(new Font("Courier", Font.BOLD, 20));
       slumberger.setBackground(Color.BLACK);
       slumberger.setForeground(Color.WHITE);
       slumberger.setFont(new Font("Courier", Font.BOLD, 20));
       custom.setBackground(Color.BLACK);
       custom.setForeground(Color.WHITE);
       custom.setFont(new Font("Courier", Font.BOLD, 20));
//       fromFile.setBackground(Color.BLACK);
//       fromFile.setForeground(Color.WHITE);
//       fromFile.setFont(new Font("Courier", Font.BOLD, 20));
     
       
       p.add(space); 

       add("South", p);
      
      RemoveAllButtons();
  
       p.add(wenner);
       p.add(slumberger);
       p.add(custom);
//       p.add(fromFile);
   
     //  add("East",previous);
     //  add(next,1000);   
       next.addActionListener(this);
       next.addMouseListener(new Adapter());
       
       previous.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PreviousActionPerformed(evt);            }        });
        
        refresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RefreshActionPerformed(evt);            }        });
                
       wennerRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wennerRefreshActionPerformed(evt);            }        });
                
       next.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NextActionPerformed(evt);            }        });
        
        clear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ClearActionPerformed(evt);           }        });
                
         menu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuActionPerformed(evt);        
            }   
        });
        
        wenner.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                WennerActionPerformed(evt);        
            }    
            });
                
         slumberger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SlumbergerActionPerformed(evt);           }        });
         
         custom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CustomActionPerformed(evt);           }        });
         
//         fromFile.addActionListener(new java.awt.event.ActionListener() {
//            public void actionPerformed(java.awt.event.ActionEvent evt) {
//                FromFileActionPerformed(evt);           }        });
         slumbergerRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                slumbergerRefreshActionPerformed(evt);           }        });
       
        hasNoArray = true;
    }
    //=================================================================================================================================================
    //=================================================================================================================================================    
    //=================================================================================================================================================
    //=================================================================================================================================================    
    //============================== PAINT METHOD ===========================\\//\\//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    public void paint(java.awt.Graphics g) {
        
                   
           if (hasNoArray) {
               for(int i = 0 ; i < 800 ; i++) {
                   g.setColor(Color.black);
                 g.drawLine(0,i,1100,i);  
                }
         
         
		
         g.drawImage(resistomatic,175,0, this);
         
        }
        if (!hasNoArray) {
            for(int i = 0 ; i < 800 ; i++) {
                   g.setColor(Color.white);
                 g.drawLine(0,i,1100,i);  
                }
      //===================== ADDING TITLE ===================== 
 
      
     // w = (wrapper) it.next();
      min = Temp.range(w,r)[0];
      max = Temp.range(w,r)[1];
       g.setColor(Color.WHITE);
       for (int i = 0 ; i < 700 ; i++) {
         g.drawLine(0,i,1100,i);  
        }
        // ===================== DRAWING BOX AROUND AREA ===================== 
               g.setColor(Color.BLACK);
        g.drawLine(0,0,0,650);
        g.drawLine(1000,0,1000,650);
        g.drawLine(0,0,1000,0);
        g.drawLine(0,650,1000,650);
        String arangement = "";
        
        // ===================== FINDING POSITIONS OF ELECTRODES ===================== 
      
        double r1 = -1;
        double r2 = -1;
        double r3 = -1;
        double r4 = -1;
        k = 0;
        Point Current1 = r.C1;
        Point Current2 = r.C2;
        Point Potential1 = w.p1;
        Point Potential2 = w.p2;
        
        
      
        
       //=====================  ARANGEMENT TYPE ===================== 
       if (w.n == 1) {
         r1 = r.r1(w.p1);  
         r3 = r.r3(w.p1);
         k = r.kValue(w.p1);
        }
        
        else {
         r1 = r.r1(w.p1);  
         r2 = r.r2(w.p2);  
         r3 = r.r3(w.p1);  
         r4 = r.r4(w.p2);  
         k = r.kValue(w.p1,w.p2);
        }
        
          if (!(r1 == -1 || r2 == -1 || r3 == -1 || r4 == -1)){
  
        if (r1 == r4 && (r2-r1 == r1)) {
            arangement = "  Arangement : WENNER";
        }
        else if (r.C1.radius(r.C2) > r1 && r.C1.radius(r.C2) > r2) {
         arangement = "  Arangement : SCHLUMBERGER";   
        }
    }
        
        
        
      // ===================== ADDING IMAGES ===================== 
       
  
       
       int yLevelDown = 182;
       int potLevel = 277;
       int intervals = 10;
       
       
       
       
       
      
      
       
       if (down == null || pot == null || rightInfinite == null || formula == null || leftInfinite == null || logo==null) {
         System.out.println("File not Found.");
         System.exit(0);
        }
        
       
        
       
       g.setColor(new Color(70,70,70));
       for (int i = 0 ; i < 30 ; i++) {
         g.drawLine(0,i,1000,i);  
        }
        
              g.setColor(Color.white);
        g.setFont(new Font("Courier", Font.BOLD, 20));
        g.drawString("Information",10,20);
        for (int i = 223 ; i <= 1000 ;i++) {
         g.drawLine(i,0,i,30);   
        }
        
       g.setColor(Color.black);
        for (int i = 0 ; i < 3 ; i++) {
            g.drawLine(0,30+i,1000,30+i);
        }
        for (int i = 220 ; i < 230 ;i++) {
         g.drawLine(i,0,i,30);   
        }
        
        g.setColor(new Color(0,128,0));
        for (int i = 0 ; i < 8 ; i ++) {
         g.drawLine(0,350+i,1000,350+i);   
        }
        g.setColor(new Color(124,68,0));
       for (int i = 0 ; i <= 30 ; i ++) {
         g.drawLine(0,358+i,1000,358+i);   
        }
        
        g.drawImage(formula,10,395,this);
        //---- drawing box for Apparent resistivity
        g.setColor(Color.BLACK);
        for (int i = 0 ; i < 63 ; i++) {
         g.drawLine(250,390+i,1000,390+i);
        }
      
       
        g.setColor(Color.WHITE);
        g.drawLine(255,395,995,395);
        g.drawLine(255,395,255,448);
        g.drawLine(255,448,995,448);
        g.drawLine(995,395,995,448);
        
   
                g.drawImage(logo,820,550,this);
        //======Drawing Scale======
        
        double distPerUnit = (max - min)/intervals;
        double DistPerPix = (max-min)/800;
        double PixPerDist = 800/(max - min);
        double startNum = min;
        
        g.setColor(Color.black);
        g.drawLine(100,100,900,100);
        for (int i = 100 ; i <= 900 ; i= i + (int) (800/intervals)) {
            g.drawLine(i,100,i,110);
        }
        
        g.drawString("DISTANCE (meters)",440,60);
        
        g.setFont(new Font("Courier", Font.PLAIN, 12));
        for (int i = 100 ; i <= 900 ; i = i + (int) (800/intervals)) {
            String s = "" + min;
           s = s.substring(0,s.indexOf('.') + 2);
            g.drawString(s, i-7,122);
            min += distPerUnit;
        }
        
        
        
        // =========== DRAWING CURRENT ELECTRODES ============
      String type = "";
      g.setColor(Color.BLACK);
      if (r.oneElectrode) {
           double C1 = r.C1.x;
           int pixelIndex = (int) (100+ (C1-startNum)*PixPerDist-18);
           g.drawImage(down,pixelIndex, yLevelDown, this);
           g.drawImage(leftInfinite, pixelIndex - 103, yLevelDown-20, this);
           g.setFont(new Font("Courier", Font.BOLD, 40));
           g.drawString("C1",pixelIndex, yLevelDown - 18);
           type = "POLE - ";
            
        }
        else {
            double C1 = r.C1.x;
            double C2 = r.C2.x;
           int pixelIndex1 = (int) (100+ (C1-startNum)*PixPerDist-18);
           g.drawImage(down,pixelIndex1, yLevelDown, this);
     
           g.setFont(new Font("Courier", Font.BOLD, 40));
           g.drawString("C1",pixelIndex1, yLevelDown - 18);
           
           int pixelIndex2 = (int) (100+ (C2-startNum)*PixPerDist-18);
           g.drawImage(down,pixelIndex2, yLevelDown, this);
           g.drawString("C2",pixelIndex2, yLevelDown - 18);
           
           type = "DIPOLE - ";
           
           for (int i = 0; i < 4 ; i++) {
             g.drawLine(pixelIndex1+18, yLevelDown + i, pixelIndex2+18, yLevelDown + i);
            }
            g.setFont(new Font("Courier", Font.PLAIN, 14));
            g.drawString("C1-C2 : r = " + r.C1.radius(r.C2), 5, yLevelDown-6);
           
        }
      // =========== DRAWING POTENTIAL ELECTRODES ==========
        
        if (w.n == 1) {
            double P1 = w.p1.x;
            int pixelIndex = (int) (100+ (P1-startNum) *PixPerDist-10);
            g.setColor(Color.WHITE);
            g.drawImage(pot,pixelIndex, potLevel, this);
            g.setColor(Color.BLACK);
            g.setColor(Color.WHITE);
            g.drawImage(rightInfinite, pixelIndex+9, potLevel-14, this);
            g.setColor(Color.BLACK);
           g.setFont(new Font("Courier", Font.BOLD, 40));
           g.drawString("P1",pixelIndex, potLevel - 13);
           type = type + "POLE ARRAY";
        }
        else {
               double P1 = w.p1.x;
            double P2 = w.p2.x;
           int pixelIndex1 = (int) (100+ (P1-startNum)*PixPerDist-10);
           g.setColor(Color.WHITE);
           g.drawImage(pot,pixelIndex1, potLevel, this);
           g.setColor(Color.BLACK);
           g.setFont(new Font("Courier", Font.BOLD, 40));
           g.drawString("P1",pixelIndex1, potLevel - 18);
           
           int pixelIndex2 = (int) (100+ (P2-startNum)*PixPerDist-18);
           g.setColor(Color.WHITE);           
           g.drawImage(pot,pixelIndex2, potLevel, this);
           g.setColor(Color.BLACK);
           g.drawString("P2",pixelIndex2, potLevel - 18);
           type = type + "DIPOLE ARRAY";
           
           for (int i = 0; i < 3 ; i++) {
             g.drawLine(pixelIndex1+8, potLevel + i, pixelIndex2+8, potLevel + i);
            }
            g.setFont(new Font("Courier", Font.PLAIN, 14));
            g.drawString("P1-P2 : r = " + w.p1.radius(w.p2), 5 , potLevel-6);
            //Index1 + java.lang.Math.abs(((pixelIndex1 - pixelIndex2)/2)) - 30), potLevel+20);
            
        }
        g.setColor(Color.black);
         g.setFont(new Font("Courier", Font.BOLD, 20));
         g.drawString(type + "   " + arangement,250,20);
        
       g.setFont(new Font("Courier", Font.BOLD, 12));
       int level = 464;
       g.drawLine(0,level-12,250,level-12);
       g.drawString("    Electrode Positions",10,level);
       level += 14;
       if (Current1 != null) {
         g.drawString("Current Electrode 1 : " + Current1.StringShort(), 10, level);
         level+=14;
        }
       if (Current2 != null) {
           g.drawString("Current Electrode 2 : " + Current2.StringShort(), 10, level);
         level+=14;
        }
       if (Potential1 != null) {
           g.drawString("Potential Electrode 1 : " + Potential1.StringShort(), 10, level);
         level+=14;
        }
       if (Potential2 != null) {
           g.drawString("Potential Electrode 2 : " + Potential2.StringShort(), 10, level);
         level+=14;
        }
        g.drawLine(0,level-12,250,level-12);
       g.drawString("    Radius",10,level);
       level += 14;
       if (r1 != -1) {
         g.drawString("C1 to P1 : r1 = " + r1,10,level);  
         level += 14;
        }
       if (r2 != -1) {
         g.drawString("C1 to P2 : r2 = " + r2,10,level);  
         level += 14;
        }
        if (r3 != -1) {
         g.drawString("C2 to P1 : r3 = " + r3,10,level);  
         level += 14;
        }
        if (r4 != -1) {
         g.drawString("C2 to P2 : r4 = " + r4,10,level);  
         level += 14;
         

        }
        g.drawLine(0,level-12,250,level-12);
        g.drawString("    Geometric Factor K ",10,level);
        level+=14;
        g.drawString("K  = " + k, 10, level);
        g.drawLine(250,452,250,650);
        
        if (r.hasVI && !hasNoArray) {
          
        double apparentResistivity = Math.abs(r.AR(k,r.I,r.V));
        double conductivity = 1/apparentResistivity;
        g.setColor(Color.BLACK);
        
          g.setFont(new Font("Courier", Font.BOLD, 12));
          
      // int level = 464;
       g.drawLine(250,level-12,500,level-12);
       g.drawLine(500,level-12,500,650);
       g.drawString("Voltage(Volts) = " + r.V,255,level);
       level += 14;
       g.drawString("Current(Amps) = " + r.I,255,level);
       level += 14;   
       g.drawString("Conductivity= " + (""+conductivity),255,level);
       g.setColor(Color.WHITE);
       
       g.setFont(new Font("Courier",Font.BOLD, 20));
       g.drawString("APPARENT RESISTIVITY(Ohm m) = " + apparentResistivity,310,428);
       g.setColor(Color.BLACK);
       g.setFont(new Font("Courier", Font.BOLD, 12));
       level = 464;
       g.drawLine(250,level-12,500,level-12);
       g.drawLine(500,level-12,500,650);
        g.drawLine(500,level-12,500,650);
        String rocks = r.rockType(apparentResistivity);
        int rockLevel = 486;
        while(rocks.length() > 2) {
          String rocksTemp = rocks.substring(0,rocks.indexOf('^'));
        g.drawString(rocksTemp,510,rockLevel);
        rocks = rocks.substring(rocks.indexOf('^')+1);
        rockLevel += 14;
        
    }
        }
        //  int level = 464;
        if (r.oneElectrode || r.C1.radius(r.C2)== 0) {
//           System.out.println("TEST");
        }
        else if (!hasNoArray) {
            g.drawLine(250,level-12,500,level-12);
       g.drawLine(500,level-12,500,650);
       g.drawString("  Depth of ",255,level);
       level += 14;
       String maxCDdepth = "" + r.C1.radius(r.C2) / java.lang.Math.pow(2,0.5);
       maxCDdepth = maxCDdepth.substring(0,6);
       g.drawString("Max Current Density = " + maxCDdepth + "m",255,level);
       level+= 14;
    }
    
    }
       
     if (wennerBoolean) { 
    	 try{
    		 
         g.drawImage(wennerImag,1,36,this);
    	 } catch(Exception e) {
    		 
    	 }
        }
     if(slumbergerBoolean) {
    	 try {
           
         g.drawImage(slumbergerImag,1,36,this);
    	 } catch(Exception e) {
    		 
    	 }
        }
      //=========== DRAWING VOLTAGE K AND APPARENT RESISTIVITY ====================
       g.setFont(new Font("Courier", Font.BOLD, 12));
       int level = 464;
       
      /* if (r.C1.radius(r.C2)== 0) {
           
        }
        else if (!hasNoArray) {
            g.drawLine(0,level-12,500,level-12);
       g.drawLine(500,level-12,500,650);
       g.drawString("  Depth of ",255,level);
       level += 14;
       String maxCDdepth = "" + r.C1.radius(r.C2) / java.lang.Math.pow(2,0.5);
       maxCDdepth = maxCDdepth.substring(0,6);
       g.drawString("Max Current Density = " + maxCDdepth + "m",255,level);
       level+= 14;
    }*/
    
     double k = -1;
          
          
//          if (w.n == 2) {
//              k = r.kValue(w.p1,w.p2);
//            }
//          else {
//            
//              k = r.kValue(w.p1);
//            
//            }
//     
     
    
    
    }
    
    class Adapter extends MouseAdapter {
    public void mouseEntered (MouseEvent e) {
       
    }
    }
    // ===================== PREVIOUS ACTION===================== 
    private void PreviousActionPerformed(java.awt.event.ActionEvent evt) {
   
          r = (Resistivity) ((QueueLink.QueueIterator) rit).previous();
       w = r.w;
          if (lastNext) {
          r = (Resistivity) ((QueueLink.QueueIterator) rit).previous();
       w = r.w;
          
           }
        lastNext = false;
       repaint();
    }
    
    //=====================  NEXT ACTION ===================== 
    public void actionPerformed (ActionEvent e) {
        hasNoArray = false;
       r = (Resistivity) rit.next();
       w = r.w;
        if (!lastNext) {
          r = (Resistivity) rit.next();
       w = r.w;
          lastNext = true;
           }
          lastNext = true;
       repaint();
    }

    private void ClearActionPerformed(java.awt.event.ActionEvent evt) {
        hasNoArray = false;
        booleanFalse();
       Cur1.setText("");
       Cur2.setText("");
       Pot1.setText("");
       Pot2.setText("");
       Volt.setText("");
       Curr.setText("");
       Volt.setText("");
       Curr.setText("");
       slumbText.setText("");
       wennerText.setText("");
       repaint();
    }
    
    
   
    private void MenuActionPerformed(java.awt.event.ActionEvent evt) {
        hasNoArray = true;
        booleanFalse();
     p.setVisible(false);
    RemoveAllButtons ();       
       p.add(wenner);
       p.add(slumberger);
       p.add(custom);
//       p.add(fromFile);
        p.setVisible(true);
        p.validate();
        repaint();

    }
    private void WennerActionPerformed(java.awt.event.ActionEvent evt) {
      hasNoArray = false;
      booleanFalse();
      RemoveAllButtons ();
      wennerBoolean = true;
      Panel pWenner = new Panel();
      p.add(wennern);
      p.add(wennerText);
      p.add(Vo);      
       p.add(Volt);
       p.add(Cu);
       p.add(Curr);
       p.add(wennerRefresh);
       p.add(clear);
     
      p.validate();
      repaint();
   
    }
    
    private void SlumbergerActionPerformed(java.awt.event.ActionEvent evt) {
        hasNoArray = false;
        booleanFalse();
        slumbergerBoolean = true;
       RemoveAllButtons ();
       p.add(wennern);
       p.add(wennerText);
       p.add(slumbn);
       p.add(slumbText);
       p.add(Vo);      
       p.add(Volt);
       p.add(Cu);
       p.add(Curr);
       p.add(slumbergerRefresh);
       p.add(clear);
       p.validate();
       repaint();
    }
    private void CustomActionPerformed(java.awt.event.ActionEvent evt) {
        hasNoArray = false;
        booleanFalse();
       p.setVisible(false);
       RemoveAllButtons ();
       p.add(refresh);
       p.add(c1);
       p.add(Cur1);
       p.add(c2);
       p.add(Cur2);
       p.add(p1);
       p.add(Pot1);
       p.add(p2);
       p.add(Pot2);
       p.add(Vo);      
       p.add(Volt);
       p.add(Cu);
       p.add(Curr);
       p.add(clear);
       p.setVisible(true);
       p.validate();
       repaint();
      

    }
    private void FromFileActionPerformed(java.awt.event.ActionEvent evt) {
        hasNoArray = false;
        booleanFalse();
        RemoveAllButtons ();
      
        p.add(previous);
          p.add(next);
          p.validate();
          repaint();
    }
    
    
    private void RefreshActionPerformed(java.awt.event.ActionEvent evt) {
        hasNoArray = false;
        booleanFalse();
       String cur1 = Cur1.getText();
       String cur2 = Cur2.getText();
       String pot1 = Pot1.getText();
       String pot2 = Pot2.getText();
       
       if (cur1.equals("")) {
           
        }
        else {
           if (cur2.equals("")) {
               double ElectrodeC1 = (new Double(cur1)).doubleValue();
               r = new Resistivity(new Point(ElectrodeC1));
            }
            else {
                double ElectrodeC1 = (new Double(cur1)).doubleValue();
                double ElectrodeC2 = (new Double(cur2)).doubleValue();
                r = new Resistivity(new Point(ElectrodeC1), new Point(ElectrodeC2));
            }
            
            if (pot1.equals("")) {
                
            }
            else {
             if (pot2.equals("")) {
                  double ElectrodeP1 = (new Double(pot1)).doubleValue();
               w = new wrapper(new Point(ElectrodeP1));
                }
             else {
                  double ElectrodeP1 = (new Double(pot1)).doubleValue();
                  double ElectrodeP2 = (new Double(pot2)).doubleValue();
                  w = new wrapper(new Point(ElectrodeP1), new Point(ElectrodeP2));
                }
            }
            String voltageString = Volt.getText();
       String currentString = Curr.getText();
       if (voltageString.equals("") || currentString.equals("")) {
           r.noVoltCurr();
        }
        else {
            double vn = (new Double(voltageString)).doubleValue();
            double cn = (new Double(currentString)).doubleValue();
            r.setVoltCurr(vn,cn);
        }
        }
        repaint();
        
       
    }
     private void NextActionPerformed(java.awt.event.ActionEvent evt) {
      
    }
    
    private void slumbergerRefreshActionPerformed(java.awt.event.ActionEvent evt) {
       
            hasNoArray = false;
        booleanFalse();
      
       String aString = wennerText.getText();
       String nString = slumbText.getText();
       
       if (aString.equals("")){
           
        }
        else {
       double a = (new Double(aString)).doubleValue();
       double nt = (new Double(nString)).doubleValue();
       double EC1 = 0;
       double EC2 = nt*a+a+nt*a;
       double EP1 = nt*a;
       double EP2 = nt*a+a;
       r = new Resistivity(new Point(EC1),new Point(EC2));
       w = new wrapper(new Point(EP1),new Point(EP2));
       
       String voltageString = Volt.getText();
       String currentString = Curr.getText();
       if (voltageString.equals("") || currentString.equals("")) {
           r.noVoltCurr();
        }
        else {
            double vn = (new Double(voltageString)).doubleValue();
            double cn = (new Double(currentString)).doubleValue();
            r.setVoltCurr(vn,cn);
        }
    }
       repaint();
        
    }
    
    private void wennerRefreshActionPerformed(java.awt.event.ActionEvent evt){
          hasNoArray = false;
        booleanFalse();
      
       String aString = wennerText.getText();
       if (aString.equals("")){
           
        }
        else {
       double a = (new Double(aString)).doubleValue();
       double EC1 = 0;
       double EC2 = 3*a;
       double EP1 = a;
       double EP2 = 2*a;
       r = new Resistivity(new Point(EC1),new Point(EC2));
       w = new wrapper(new Point(EP1),new Point(EP2));
       
       String voltageString = Volt.getText();
       String currentString = Curr.getText();
       if (voltageString.equals("") || currentString.equals("")) {
           r.noVoltCurr();
        }
        else {
            double vn = (new Double(voltageString)).doubleValue();
            double cn = (new Double(currentString)).doubleValue();
            r.setVoltCurr(vn,cn);
        }
    }
       repaint();
    }
    
    private void RemoveAllButtons () {
         p.setVisible(false);
       p.remove(slumberger);
       p.remove(wenner);
       p.remove(next);
       p.remove(previous);
       p.remove(custom);
       p.remove(refresh);
       p.remove(c1);
       p.remove(Cur1);
       p.remove(c2);
       p.remove(Cur2);
       p.remove(p1);
       p.remove(Pot1);
       p.remove(p2);
       p.remove(Pot2);
       p.remove(Vo);      
       p.remove(Volt);
       p.remove(Cu);
       p.remove(Curr);
       p.remove(clear);
       p.remove(previous);
       p.remove(next);   
//       p.remove(fromFile);
       p.remove(wennerText);
       p.remove(wennern);
       p.remove(slumbText);
       p.remove(slumbn);
       p.remove(slumbergerRefresh);
       p.remove(wennerRefresh);
        p.setVisible(true);
        
        
        
    }
    
    
    
    private void booleanFalse () {
     wennerBoolean = false;
     menuBoolean = false;
     slumbergerBoolean = false;
     r.noVoltCurr();
    }


public static class Parser   {

    
    public static void print () {
        String st = DataToString("DATA.txt");
        System.out.println(st);
    }
    
    public static QueueLink parse() {
        QueueLink q = new QueueLink();
        String data = DataToString("DATA.txt");
        boolean hasC1 = data.indexOf("C1") != -1;
        boolean hasC2 = data.indexOf("C2") != -1;
        boolean hasP1 = data.indexOf("P1") != -1;
        boolean hasP2 = data.indexOf("P2") != -1;
        boolean hasI  = data.indexOf("I")  != -1;
        boolean hasV  = data.indexOf("V")  != -1;
        boolean C2Null = false;
        //System.out.println("Print data");
        //System.out.println(data);
        //System.out.println("Removing header");
        
        // finding first index of new Line
        int indexNewLine = data.indexOf("\r\n");
        data = data.substring(indexNewLine+2);
        //System.out.println(data);
        
        //index of temporary tab
        int indexNewTab = data.indexOf("\t");
        
        while (!data.equals("") && data.length() > 4) {
            indexNewLine = data.indexOf("\r\n");
            indexNewTab = data.indexOf("\t");
            
  
            
         Resistivity r = null;
         Point C1 = null;
         Point C2 = null;
         wrapper w = null;
         double V = 0;
         double I = 0;
           
             //System.out.println("taking C1");
             String c1 = data.substring(0,indexNewTab);
             data = data.substring(indexNewTab+1);
             indexNewTab = data.indexOf("\t");
             //System.out.println(data);
             //System.out.println(c1);
             if (c1.equals("") || !hasNumber(c1)) {
                 
                }
             else {
                C1 = new Point(toDouble(c1));
                
                }
             
             
             //System.out.println("taking C2");
             String c2 = data.substring(0,indexNewTab);
             data = data.substring(indexNewTab+1);
             indexNewTab = data.indexOf("\t");
             //System.out.println(data);
             //System.out.println(c2);
             if (c2.equals("")|| !hasNumber(c2)) {
                 C2Null = true;
                }
             else {
                C2 = new Point(toDouble(c2));
                
                }
             
             
             //System.out.println("taking p1");
             String p1 = data.substring(0,indexNewTab);
             data = data.substring(indexNewTab+1);
             indexNewTab = data.indexOf("\t");
             //System.out.println(data);
             //System.out.println(p1);
             if (p1.equals("")|| !hasNumber(p1)) {
                 
                }
             else {
                w = new wrapper(new Point(toDouble(p1)));
                
                }
             
             //System.out.println("taking p2");
             String p2 = data.substring(0,indexNewTab);
             data = data.substring(indexNewTab+1);
             indexNewTab = data.indexOf("\t");
             //System.out.println(data);
             //System.out.println(p2);
             if (p2.equals("")|| !hasNumber(p2)) {
                 
                }
             else {
               w.p2 = new Point(toDouble(p2));
               w.n=2;
                
                }
             
             //System.out.println("taking Voltage");
             String vo = data.substring(0,indexNewTab);
             data = data.substring(indexNewTab+1);
             indexNewTab = data.indexOf("\t");
             //System.out.println(data);
             //System.out.println(vo);
             if (vo.equals("")|| !hasNumber(vo)) {
                 
                }
             else {
                V = toDouble(vo);
                
                }
             
             if (!data.equals("")) {
             indexNewLine = data.indexOf("\r\n");
             //System.out.println("taking Current");
             String cu = data.substring(0,indexNewLine+1);
             data = data.substring(indexNewLine+1);
             indexNewTab = data.indexOf("\t");
             //System.out.println(data);
             //System.out.println(cu);
             if (cu.equals("") || !hasNumber(cu)) {
                 
                }
             else {
                I = toDouble(cu);
                
                }
            }
            if (!C1.equals(null)) {
                if(C2Null) r = new Resistivity(C1);
                else {
                r = new Resistivity(C1,C2);
            }
            r.w = w;
            if (V!=0 && I!=0) {
              r.V = V;
              r.I = I;
              r.hasVI = true;
            }
            else {
             r.hasVI = false;   
            }
            q.enqueue(r);
            }
            
            
            
             ////System.out.println(data);
             
          
        
        }
          return q;
    }
    
    public static double toDouble(String s) {
        double d = (new Double(s)).doubleValue();
        return d;
    }
    
    public static boolean hasNumber(String s) {
     boolean b = false;
     for (int i = 0 ; i <= 9 ; i++) {
         b = b || (s.indexOf(""+i) != -1);
        }
        return b;
    }
    
    
    
    public static void main () {
        Resistivity [] list = {new Resistivity(null)};
        QueueLink arrays = new QueueLink();
        Point C1 = new Point(0);
        Point C2 = new Point(1);
        double P1 = 0;
        double P2 = 0;
        double Voltage = 0;
        double Current = 0;
        int n= 0;
        
        
        double C1Temp;
        double C2Temp;
        double P1Temp;
        double P2Temp;
        double VTemp;
        double ITemp;

        
        wrapper w = new wrapper (new Point (0));
        String data = DataToString("DATA.txt");
        //----------------------
        System.out.println(data);
        System.out.println("finished printing data");
        //----------------------
        int length = data.length();
        int indexTab;
        int indexNewLine;
        int indexR;
        int index = 0;
        boolean isFinished = false;
        boolean hasC1 = data.indexOf("C1") != -1;
        boolean hasC2 = data.indexOf("C2") != -1;
        boolean hasP1 = data.indexOf("P1") != -1;
        boolean hasP2 = data.indexOf("P2") != -1;
        boolean hasI  = data.indexOf("I")  != -1;
        boolean hasV  = data.indexOf("V")  != -1;
        boolean hasVoltage = false;
        boolean hasCurrent = false;
        boolean hasPotential = false;
         Resistivity r = null;
        data = data.substring(data.indexOf("\n") +1);
        
        //----------------------
        System.out.println("Removed header");
        System.out.println(data);
        //----------------------
        while (!isFinished) {
           
           indexTab = data.indexOf("\t");
           indexNewLine = data.indexOf("\n");
           indexR = data.indexOf("\r");
                      
           if (indexR == -1) {
             isFinished = true;  
            }
            else {
                if(data.substring(0,1).equals("\n")) {
                 data = data.substring(1);   
                }
              if (hasC1) {
                  int tempIndex = java.lang.Math.min(data.indexOf("\t"),data.indexOf("\n"));
                  
                 String temp = data.substring(0,tempIndex);
                 if (!temp.equals("")) {
                 C1Temp = (new Double(temp)).doubleValue();
                 data = data.substring(tempIndex+1);
                 System.out.println(data);
                 C1 = new Point(C1Temp);
                 r = new Resistivity(C1);
                 
                }
                else {
                 C1 = null;   
                 
                }
                if(data.substring(0,1).equals("\t")||data.substring(0,1).equals("\n")) {
                 data = data.substring(1);   
                }
                
                 
                }
                if(data.substring(0,1).equals("\n")) {
                 data = data.substring(1);   
                }
              if (hasC2) {
                  int tempIndex = java.lang.Math.min(data.indexOf("\t"),data.indexOf("\n"));
                  String temp = data.substring(0,tempIndex);
                  if (!temp.equals("")) {
                 C2Temp = (new Double(temp)).doubleValue();
                 data = data.substring(tempIndex+1);
                 System.out.println(data);
                 C2 = new Point(C2Temp);
                 r.C2 = C2;
                 r.oneElectrode = false;
                }
                else {
                 C2=null;   
                }
                }
             
              
              //  Resistivity rTemp = new Resistivity (C1,C2);
                
                if(data.substring(0,1).equals("\n")) {
                 data = data.substring(1);   
                }
                
               if (hasP1) {
                   int tempIndex = java.lang.Math.min(data.indexOf("\t"),data.indexOf("\n"));
                   String temp = data.substring(0,tempIndex);
                   if (!temp.equals("")) {
                 P1Temp = (new Double(temp)).doubleValue();
                 data = data.substring(tempIndex+1);
                 P1 = P1Temp;
                 w = new wrapper(new Point(P1));
                 hasPotential = true;
                }
                
                else {
                w = null;   
                hasPotential = false;
                }
                }
                if(data.substring(0,1).equals("\n")) {
                 data = data.substring(1);   
                }
               if (hasP2) {
                int tempIndex = java.lang.Math.min(data.indexOf("\t"),data.indexOf("\n"));
                  String temp = data.substring(0,tempIndex);
                  if (!temp.equals("")) {
                 P2Temp = (new Double(temp)).doubleValue();
                 data = data.substring(tempIndex+1); 
                 P2 = P2Temp;
                 w.p2 = new Point(P2);
                 w.n = 2;
                 
                }
                else {
                 w = new wrapper(new Point(P1));
                 w.p2 = null;   
                 w.n=1;
                }
                }
                
                
                
                if(data.substring(0,1).equals("\n")) {
                 data = data.substring(1);   
                }
               
                
               if (hasV) {
                   int tempIndex = java.lang.Math.min(data.indexOf("\t"),data.indexOf("\n"));
                   String temp = data.substring(0,tempIndex);
                   if (!temp.equals("")) {
                 VTemp = (new Double(temp)).doubleValue();
                 data = data.substring(tempIndex+1);
                 Voltage = VTemp;
                 
                 hasVoltage = true;
                }
                else {
                hasVoltage = false;    
                }
                }
                if(data.substring(0,1).equals("\n")) {
                 data = data.substring(1);   
                }
                
                if (hasI) {
                   int tempIndex = java.lang.Math.min(data.indexOf("\t"),data.indexOf("\n"));
                   String temp = data.substring(0,tempIndex);
                   if (!temp.equals("")) {
                 ITemp = (new Double(temp)).doubleValue();
                 data = data.substring(tempIndex+1);
                 Current = ITemp;
                 
                 hasCurrent = true;
                }
                else {
                    hasCurrent = false;
                    }
                }
                if (r.equals(null)) {
                   
                }
                if (!r.equals(null)) {
                
                
                r.w = w;
                
                if (hasVoltage && hasCurrent) {
                    r.V = Voltage;
                    r.I = Current;
                }
                arrays.enqueue(r);
            }
                
                
                
              //  r = new Resistivity(null);
                if (data.equals("\t") || data.equals("\t\n") || data.equals("\n")) {
                    isFinished = true;
                }
                System.out.println(data);
                if(n == 2) {
                 isFinished = true;   
                }
                n++;
               if(data.substring(0,1).equals("\n")) {
                 data = data.substring(1);   
                }
            }
            
            
           
        }
        
       
       
        
      
        
    }
    
    public static String [][] split(String st, String Column, String NewLine) {
        int numColumns = 0;
        int numRows = 0;
        int rowTemp = 0;
        int colTemp = 0;
        String temp = st;
        while (!temp.equals("")) {
         int indexRow = temp.indexOf(NewLine);   
         if (indexRow == -1) {
             temp.equals("");
            }
         else {
             numRows++;
             temp = temp.substring(indexRow);
            }
        }
        temp = st;
        int indexRow = temp.indexOf(NewLine);
           temp = temp.substring(0, indexRow);
        while(!temp.equals("")) {
           int indexColumn = temp.indexOf(Column);
           if (indexColumn == -1) {
             temp = "";  
            }
            else {
             temp = temp.substring(indexColumn);
             numColumns++;
            }
           
        }
        temp = st;
        
        
        String [][] array = new String [numRows][numColumns];
        
        while(!temp.equals("")) {
           int indexColumn = temp.indexOf(Column);
           if (indexColumn == -1) {
             temp = "";  
            }
            else {
                String startTemp = temp.substring(0,indexColumn);
             temp = temp.substring(indexColumn);
             
             numColumns++;
            }
           
        }
        
      
        
        return array;
        
    }




public static String DataToString (String file) {
String theText= "";
if (file.length() < 1) {
  //  System.out.println("Input File.");
    System.exit(0);

    }
    File f = new File(file);
    int len = (int) f.length();
    byte ascii[] = new byte[len];
    try {
    // RandomAccessFile rf= new RandomAccessFile(f,"rw");
     FileInputStream fis = new FileInputStream(f);
     
     fis.read(ascii,0,len);
     theText = new String(ascii);
     //System.out.println(theText);
     return theText;
    }
    catch (IOException e){        
        System.out.println(e);
    }
    return theText;
    }
    
   
    
    
    
    
    
    

    
}
    
    
    
    
    }