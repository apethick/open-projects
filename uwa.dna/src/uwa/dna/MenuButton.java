package uwa.dna;
import java.awt.*;
import java.awt.*;
/**
 * Class for a simple button
 * 
 * @author Daniel McGregor
 * @version 1.00
 */
public class MenuButton
{
    private int x;
    private int y;
    private int height;
    private int width;
    private String label;
    private int id;
    private Image img;

    /**
     * Constructor for objects of class MenuButton
     */
    public MenuButton(int id, String label, int x, int y, int width, int height, Image img)
    {
        this.id = id;
        this.label = label;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.img = img;
    }
    
    public void setImage(Image img) {
        this.img = img;
    }

    public void setWidth(int w) {
        this.width = w;
    }

    public void setHeight(int h) {
        this.height = h;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }    

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }     
    
    /** returns true if point is on button
     * 
     */
    public boolean isPoint(int posX, int posY)
    {
        if (x <= posX && posX <= x + width && y <= posY && posY <= y + height)
            return true;
        else
            return false;
    }
    
    /** draw button on sc at point x,y with a width and height
     * 
     */
    public void drawButton(SimpleCanvas sc) {
        sc.drawImage(img , x, y, width, height);
    }
}
