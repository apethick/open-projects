package uwa.dna;
import java.net.URL;
import javax.swing.ImageIcon;
import java.awt.Image;

public class ImageLoader
{
    private Image loadedImage;
    private ImageIcon loadedIcon;

    public ImageLoader()
    {
    }
    
    public Image loadImage(URL url) {
        loadedIcon = new ImageIcon(url);
        loadedImage = loadedIcon.getImage();
        return loadedImage;
    }
}
