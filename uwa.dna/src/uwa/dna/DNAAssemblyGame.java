package uwa.dna;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.*;
import java.net.URL;
import java.applet.*;
import java.awt.event.ComponentListener;
import java.awt.event.ComponentEvent;


/**
 * Write a description of class DNAAssemblyGame here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class DNAAssemblyGame implements MouseListener, ComponentListener
{
    private static int MAIN_MENU = 1;
    private static int DIFFICULTY_MENU = 2;
    private static int ABOUT_MENU = 3;
    private int currentMenu = 1;
    
    private DNAPuzzle puzzle;
    private SimpleCanvas menu;
    private MenuButton[] buttons;
    private MenuButton[] mainMenuButtons;
    private MenuButton[] aboutButtons;
    
 
    
    private int numberOfButtons = 5;
    private int numberOfMainMenuButtons = 7;
    private int numberOfAboutButtons = 2;
    
    private ImageLoader il;
    
    private URL menuBackURL = this.getClass().getResource("img/startup.png");  
    private URL diffSelectURL = this.getClass().getResource("img/DifficultySelection.gif");  
    private URL easyURL = this.getClass().getResource("img/EasyGame.gif"); 
    private URL mediumURL = this.getClass().getResource("img/MediumGame.gif"); 
    private URL hardURL = this.getClass().getResource("img/HardGame.gif"); 
    private URL quitURL = this.getClass().getResource("img/Quit.gif");  
    private URL backURL = this.getClass().getResource("img/back.gif");
    private URL quickGameURL = this.getClass().getResource("img/quickGame.gif"); 
    private URL newGameURL = this.getClass().getResource("img/newGame.gif"); 
    private URL demonstrationURL = this.getClass().getResource("img/demonstration.gif"); 
    private URL helpURL = this.getClass().getResource("img/help.gif");  
    private URL aboutURL = this.getClass().getResource("img/about.gif");
    private URL creditsURL = this.getClass().getResource("img/credits.gif");
    private URL levelURL = this.getClass().getResource("img/level.gif");    
    
    private Image easyImg, mediumImg, hardImg, quitImg, backImg, quickGameImg, aboutImg, creditsImg;
    private Image newGameImg, demonstrationImg, helpImg, menuBackImg, diffSelectImg, levelImg;
    
    private Thread thread;
    
    public DNAAssemblyGame() 
    {
             thread = null;
        loadImages();         //prepares button images for rendering 
        menu = new SimpleCanvas("DNA Fragment Assembly Game", 800, 600);         //sets up canvas
        menu.addMouseListener(this);         //adds mouse listener
        menu.addComponentListener(this);    //adds listener for resize Events
        initMenu(); //set up buttons
        drawMenu();
    }

    private void loadImages() {
       il = new ImageLoader();
       quickGameImg = il.loadImage(quickGameURL);
       newGameImg = il.loadImage(newGameURL);
       demonstrationImg = il.loadImage(demonstrationURL);
       helpImg = il.loadImage(helpURL);
       menuBackImg = il.loadImage(menuBackURL);
       diffSelectImg = il.loadImage(diffSelectURL);
       easyImg = il.loadImage(easyURL);
       mediumImg = il.loadImage(mediumURL);
       hardImg = il.loadImage(hardURL);
       quitImg = il.loadImage(quitURL);
       backImg = il.loadImage(backURL); 
       aboutImg = il.loadImage(aboutURL);
       creditsImg = il.loadImage(creditsURL);
       levelImg = il.loadImage(levelURL);       
    }        
    
    private void initMenu() {
        
        buttons = new MenuButton[numberOfButtons];
        mainMenuButtons = new MenuButton[numberOfMainMenuButtons];
        aboutButtons = new MenuButton[numberOfAboutButtons];
        //setup button ids, name, and x,y pos, and width, height and image
        
        mainMenuButtons[0] = new MenuButton(0, "Quick Game", 500, 49, 212, 77, quickGameImg);
        mainMenuButtons[1] = new MenuButton(1, "New Game", 500, 149, 212, 77, newGameImg);
        mainMenuButtons[2] = new MenuButton(2, "Demonstration", 500, 249, 212, 77, demonstrationImg);
        mainMenuButtons[3] = new MenuButton(3, "Help", 500, 349, 212, 77, helpImg);   
        mainMenuButtons[4] = new MenuButton(4, "Quit", 5, 5, 161, 68, quitImg); 
        mainMenuButtons[5] = new MenuButton(5, "About", 530, 450, 141, 40, aboutImg);
        mainMenuButtons[6] = new MenuButton(6, "Level", 530, 500, 141, 40, levelImg);        
        
        buttons[0] = new MenuButton(0, "Easy Game", 500, 150, 204, 94, easyImg);
        buttons[1] = new MenuButton(1, "Medium Game", 500, 250, 204, 94, mediumImg);
        buttons[2] = new MenuButton(2, "Hard Game", 500, 350, 204, 94, hardImg);
        buttons[3] = new MenuButton(3, "Quit", 5, 5, 161, 68, quitImg);        
        buttons[4] = new MenuButton(4, "Back", 530, 500, 185, 63, backImg);   
        
        aboutButtons[0] = new MenuButton(0, "Quit", 5, 5, 161, 68, quitImg);
        aboutButtons[1] = new MenuButton(1, "Back", 530, 500, 185, 63, backImg);       
        
    }
    
    private void drawMenu() {
        menu.setForegroundColour(new Color(255,255,255));
        
        menu.clear(); //clear screen
        menu.drawImage(menuBackImg , 0, 0);
        menu.repaint(); //refreshscreen
        if (currentMenu == MAIN_MENU) {
              for (int i = 0; i < numberOfMainMenuButtons; i++)
                  mainMenuButtons[i].drawButton(menu); //draw buttons
        }
        else if (currentMenu == DIFFICULTY_MENU) {
              menu.drawImage(diffSelectImg , 375, 50);  
              for (int i = 0; i < numberOfButtons; i++)
                    buttons[i].drawButton(menu); //draw buttons
        }
        else if (currentMenu == ABOUT_MENU) {
                for (int i = 0; i < numberOfAboutButtons; i++)
                 aboutButtons[i].drawButton(menu); //draw buttons
        
             menu.setForegroundColour(new Color(0,0,0));
            menu.drawImage(creditsImg, 470,5);
            
               
        }

        menu.repaint(); //refreshscreen
    }
    
    public void mouseClicked(MouseEvent e) {
    }
    
    private void startGame(Settings s) {
        puzzle = new DNAPuzzle(s); //create new puzzle/level with settings defined in s
        puzzle.addMenu(menu); //let puzzle have reference to this menu so it can show it again
        puzzle.drawDNA(); //show puzzle
        menu.hide(); //hide menu
        currentMenu = MAIN_MENU;
        drawMenu();
    }
    
     public void componentResized(ComponentEvent e) {
       int oldWidth = menu.getWidth();
        int oldHeight = menu.getHeight();
        if (e.getComponent().getWidth() < 800 || e.getComponent().getHeight() < 600)
            return;
        menu.updateSize();
        int newWidth = menu.getWidth();
        int newHeight = menu.getHeight();
        int currentX = 0;
        int currentY = 0;
        int currentWidth = 0;
        int currentHeight = 0;
        double widthRatio = (double) newWidth / (double) oldWidth;
        double heightRatio = (double) newHeight / (double) oldHeight;
        drawMenu();
        
    }
    
    
       
    public void componentMoved(ComponentEvent e) {
    }
    
    public void componentShown(ComponentEvent e) {
    }

    public void componentHidden(ComponentEvent e) {
    }
    
    
    public void mousePressed(MouseEvent e)
    {
    }
    
    public void mouseReleased(MouseEvent e) {
        int button = -1;
        int x = e.getX();
        int y = e.getY();
        
    
        
        if (currentMenu == MAIN_MENU) {
            for (int i = 0; i < numberOfMainMenuButtons; i++) { //check to see if a button was clicked
                if(mainMenuButtons[i].isPoint(x, y)) {
                    button = i;
                    break;
                }        
            }

            switch (button) {
            case 0:
                startGame(Settings.EASY); //button 0 clicked start an Easy Game
                break;
            case 1:
                currentMenu = DIFFICULTY_MENU; 
                drawMenu();
                break;
            case 2:
                startGame(Settings.MEDIUM);
                puzzle.startDemo();
                break;               
            case 3:
                Help helpWindow = new Help();
                break;                
            case 4:
                System.exit(0); //quit clicked, exit
                break;              
            case 5:
                currentMenu = ABOUT_MENU;
                drawMenu();
                break;
            case 6:
                Level addLevel = new Level();
                addLevel.admin();
                break;
            
            default:
                break;
            }
        }
        else if(currentMenu == DIFFICULTY_MENU) {
            for (int i = 0; i < numberOfButtons; i++) { //check to see if a button was clicked
                if(buttons[i].isPoint(e.getX(), e.getY())) {
                    button = i;
                    break;
                }
            }
            switch (button) {
            case 0:
                startGame(Settings.EASY); //button 0 clicked start an Easy Game
                break;
            case 1:
                startGame(Settings.MEDIUM);  //button 1 clicked start a Medium Game
                break;
            case 2:
                startGame(Settings.HARD);  //button 2 clicked start a hard game
                break;                
            case 3:
                System.exit(0); //quit clicked, exit
                break;     
            case 4 :
                currentMenu = MAIN_MENU; 
                drawMenu();
                break;
            default:
                break;
        }
         
      }
        else if (currentMenu == ABOUT_MENU) {
            for (int i = 0; i < numberOfAboutButtons; i++) { //check to see if a button was clicked
                if(aboutButtons[i].isPoint(x, y)) {
                    button = i;
                    break;
                }        
            }
            
             switch (button) {
            case 0:
               
                break;
            case 1:
                currentMenu = MAIN_MENU; 
                drawMenu();
                break;
                     
            default:
                break;
            }
 
        }
    }
    
    public void mouseEntered(MouseEvent e)
    {
    }
    
    public void mouseExited(MouseEvent e) 
    {
    }   

}
