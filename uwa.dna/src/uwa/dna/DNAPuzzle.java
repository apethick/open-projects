package uwa.dna;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.ComponentListener;
import java.awt.event.ComponentEvent;
import java.util.Random;
import java.awt.*;
import java.net.URL;
import java.applet.*;
import javax.swing.JFrame;
import javax.swing.Timer;
/**
 * DNAPuzzle
 * Main Class used to start a DNA Fragment 'level'
 * 
 * @author Daniel McGregor
 * @version 0.1
 * @date 10/04/2007
 */
public class DNAPuzzle implements MouseListener, MouseMotionListener, ComponentListener, ActionListener
{
    //local variables
    private SimpleCanvas game, menu;        // two canvas's
    private DNA pz;                         // object to hold a collection of DNA fragments
    private int width;
    private int height;
    private int xOff, yOff;    
    private int peice;
    private int numGameButtons = 4;
    private double mutationRatePer;
    private boolean dragging = false;
    private boolean won = false;
    private boolean running = true;
    private Timer animationTimer;
    private Timer wonTimer;    
    private int animationTimeToMovePeice = 3000;
    private int movements = 30;
    private int animationDelay = animationTimeToMovePeice / movements;
    private int wonDelay = 3000;
    private DNAFragment destAutoMove;
    private DNAFragment origAutoMove;
    private int xAutoMove = 0;
    private int yAutoMove = 0;
    private int autoMovements = 0;
    private int autoPeices = 0;
    private int buttonX = 590;
    private int buttonY = 540;
    private int buttonWidth = 100;
    private int buttonHeight = 45;    
    private boolean autoMove = false;
    private boolean solveAll = false;
    private boolean demo = false;
    private boolean isPlayed = false;
      
    private boolean soundOn = true;
    private boolean speechOn = true;
    
    private String levelName;
    
    private Random generator = new Random();
    private Settings settings;
    private ImageLoader il = new ImageLoader();
    
    private URL backgroundURL = this.getClass().getResource("img/dna.jpg");
    private URL congratsURL = this.getClass().getResource("img/congrats.gif"); 
    private URL exitURL = this.getClass().getResource("img/exit.gif");
    private URL soundOffURL = this.getClass().getResource("img/SoundOff.gif");
    private URL speechOffURL = this.getClass().getResource("img/SpeechOff.gif");
    private URL soundOnURL = this.getClass().getResource("img/SoundOn.gif");
    private URL speechOnURL = this.getClass().getResource("img/SpeechOn.gif");
    private URL hintURL = this.getClass().getResource("img/hint.gif");   
    private URL solveURL = this.getClass().getResource("img/solve.gif");
    private URL winWavURL = this.getClass().getResource("sounds/win.wav");
    private URL clickWavURL = this.getClass().getResource("sounds/pickup.wav");

    private URL musicURL = this.getClass().getResource("sounds/music.mid");
    
    private AudioClip winAudio, clickAudio, music;   
    private Image backgroundImg, congratsImg, exitImg, hintImg, solveImg, speechOffImg, soundOffImg, speechOnImg, soundOnImg;
    private MenuButton[] gameButtons; // exitButton, hintButton, solveButton, speechButton, soundButton;
    /**
     * Constructor for DNAPuzzle
     * This constructor creates an object based on the parameters supplied by s
     */
    public DNAPuzzle(Settings s) {
        settings = new Settings(s); // creates a copy 
        width = settings.width;     //sets local variable (obsolete)
        height = settings.height;   //sets local variable (obsolete);
        loadImages();
        game = new SimpleCanvas("DNA Fragment Game", s.width, s.height); //creates SimpleCanvas which is used for drawing graphics
        
        game.addComponentListener(this);    //adds listener for resize Events
        game.addMouseListener(this);        //adds listener for mouse clicks
        game.addMouseMotionListener(this);  //adds listener for mouse movement
       
        game.repaint();
        initDNA();
        initButtons();
        //initialise DNA fragments
        drawDNA();                          //render screen

    }
    
    public DNAPuzzle()
    {
        this(Settings.EASY);
    }

    public void loadImages() {
        backgroundImg = il.loadImage(backgroundURL);
      
        exitImg = il.loadImage(exitURL);
        hintImg = il.loadImage(hintURL);
        solveImg = il.loadImage(solveURL);
        speechOffImg = il.loadImage(speechOffURL);
        soundOffImg = il.loadImage(soundOffURL);
        speechOnImg = il.loadImage(speechOnURL);
        soundOnImg = il.loadImage(soundOnURL);
        congratsImg = il.loadImage(congratsURL);
        winAudio = Applet.newAudioClip(winWavURL);
        clickAudio = Applet.newAudioClip(clickWavURL);
        music = Applet.newAudioClip(musicURL);
    }
    
    /**
     * Returns settings object for inspection/modification
     */
    public Settings getSettings() {
        return settings;                    //to obtain settings object
    }

    /**
     * Provides link between game SimpleCanvas and "Parent Menu" SimpleCanvas
     * Probably a better way of doing this but I cant think of anything at the momment - Daniel M 11/04/07 12:28 AM
     */
    public void addMenu(SimpleCanvas menu) {
        this.menu = menu;
    }
    
    /**
     * returns Puzzle Width
     */
    public int getPuzzleWidth() {
        return width;
    }
    
    /** returns Puzzle Height
     * 
     */
    public int getPuzzleHeight() {
        return height;
    }
    
    /** Provides access to canvas object
     * 
     */
    public SimpleCanvas getCanvas() {
        return game;
    }
    
    private void solve(DNAFragment peice1, DNAFragment peice2) {
        if (!autoMove) {
        destAutoMove = peice1;
        origAutoMove = peice2;
        autoMovements = movements;
        autoMove = true;
        animationTimer = new Timer(animationDelay, this);
        animationTimer.start();
        }
    }
    
    public void solve() {
        int peice1 , peice2;
        
        peice2 = Math.abs(generator.nextInt()) % (settings.peices - 1) + 2;
        peice1 = peice2 - 1;
        dragging = true;
        peice = peice2;
        
        autoPeices = pz.getPeices();
        try {
            solve(pz.getFragment(peice1), pz.getFragment(peice2));
        }
        catch (Exception e) {}
    }
    
    public void startDemo() {
        demo = true;
        solveAll();
    }
    
    public void solveAll() {
        if (!autoMove) {
            solveAll = true;
            solve();
        }
    }
       
    /** Debuging function for displaying details about fragments
     * 
     */
    public void printStat() {

        for (int i = 1; i <= pz.getPeices(); i++) {
            try {
            System.out.print("DNA: " + pz.getFragment(i).getDNAFragment() + ", ");
            System.out.print("X: " + pz.getFragment(i).getX() + ", "); 
            System.out.print("Y: " + pz.getFragment(i).getY() + ", ");            
            System.out.print("Y: " + pz.getFragment(i).getSequence() + "\n");            
            }
            catch (Exception e) {}
        
        }
    }
    
    /** Initializes DNA fragments
     * 
     */
    public void initDNA() {
        int xUnits = 0;
        int yUnits = 0;
        pz = new DNA(settings); //creates new DNA Fragment Collection
        levelName = pz.getLevelName();
        mutationRatePer = (double) settings.mutationRate  / (double) settings.maxMutationRate * 100.0;
        int tempX;              //temp variable for X position
        int tempY;              //temp variable for Y position
        for (int i = 1; i <= pz.getPeices(); i++) {
            try {
                xUnits = (width - pz.getFragment(i).getWidth()); // / settings.cellSizeH;
                yUnits = (height - pz.getFragment(i).getHeight() - settings.bottomBorder); // / settings.cellSizeV;
                
                tempX = (Math.abs(generator.nextInt()) % xUnits);// * settings.cellSizeH;    // random x position within width of game canvas
                tempY = (Math.abs(generator.nextInt()) % yUnits);// * settings.cellSizeV;    // random y position within height of game canvas        
                pz.getFragment(i).setX(tempX);      //set fragment x pos
                pz.getFragment(i).setY(tempY);      //set fragment y pos
            }
            catch (Exception e) {}
        }
    }
    
    public void initButtons() {
        buttonX = (int) ((double) settings.width * 0.7375);
        buttonY = (int) ((double) settings.height * 0.9);
        gameButtons = new MenuButton[numGameButtons];
       
        gameButtons[0] = new MenuButton(0, "Back", buttonX, buttonY, buttonWidth, buttonHeight, exitImg);
        gameButtons[1] = new MenuButton(1, "Hint", buttonX - buttonWidth, buttonY - buttonHeight - 5, buttonWidth, buttonHeight, hintImg);
        gameButtons[2] = new MenuButton(2, "Solve", buttonX - buttonWidth, buttonY, buttonWidth, buttonHeight, solveImg);
        gameButtons[3] = new MenuButton(3, "Sound", buttonX, buttonY - buttonHeight - 5, buttonWidth, buttonHeight, soundOnImg);       
    }
    
    /**
     * drawDNA on game canvas
     */
    public void drawDNA() {
        int textH = 10;
        int textV = settings.height - settings.bottomBorder - 60;
        int textHeight = 15;

        if (game != null) {
            game.drawBackground(backgroundImg);     //render backgroundImg onto game canvas
            game.setForegroundColour(Color.black);

            game.drawString("Sequence : " + levelName, textH, textV);
            game.drawString("Difficulty : " + settings.description, textH, textV + textHeight);
            game.drawString("Overlap : " + settings.overlap, textH, textV + textHeight * 2);
            game.drawString("Error Rate : " + mutationRatePer + "%", textH, textV + textHeight * 3);

            for (int i = 1; i <= pz.getPeices(); i++) { //loop thru peices and draw on game canvas
                try {
                    pz.getFragment(i).drawOnCanvas(game); //draw fragment i on canvas game
                }
                catch (Exception e)
                {
                    System.out.println();  //exception catch, shouldnt ever get here
                }
            }
        
            if (won)
                wonGame();          // if in won state draw wonGame instead

                if (peice > 0 && dragging) { //if dragging a peice draw it on top
                    try {
                        pz.getFragment(peice).drawOnCanvas(game); //draw fragment i on canvas game
                    }
                    catch (Exception e) {
                        System.out.println();  //exception catch
                    }
                }
        
                for (int j = 0; j < numGameButtons; j++) {
                    gameButtons[j].drawButton(game);
                }
                game.repaint(); //refresh screen
        }
    }
    
    /** called when mouse dragging event is captured by MouseMotionListener
     * 
     */
    public void mouseDragged(MouseEvent e) {
        if (dragging && !won & !autoMove) { //if dragging a peice is true & we have not won and are moving peices automatically
            try {
                pz.getFragment(peice).setX(e.getX() + xOff); //update fragment peice relative to mouse X
                pz.getFragment(peice).setY(e.getY() + yOff); //update fragment peice relative to mouse Y
                drawDNA();
            }
            catch (Exception ex) {} //exception catch, shouldnt ever get here
        }
    }

    public void mouseMoved(MouseEvent e) { //needed for MouseMotionListener

                    
    }
    
    public void mouseClicked(MouseEvent e) //needed for MouseListener
    {
    }
    
    /** called when mousePressed event is captured by MouseListener
     * 
     */
    public void mousePressed(MouseEvent e)
    {
     
        dragging = false; //set dragging state to off
        peice = pz.isPeice(e.getX(), e.getY()); //ask pz if co - ordinates corrospond to a peice
        if (peice > 0 && !autoMove) { // if they do
            dragging = true; //we are dragging
            try {
                xOff = pz.getFragment(peice).getX() - e.getX(); //update fragment x pos
                yOff = pz.getFragment(peice).getY() - e.getY(); //update frament y pos
            }
            catch (Exception ex) { System.out.println("Error!"); }
            if (soundOn) {
                try {
                    clickAudio.play();
                }
                catch (Exception f) {
                    f.printStackTrace();
                }
            }
        }
    }
    
    public boolean isRunning() {
        return running; // returns game state
    }
    
    
    /** Render won game screeen
     *  Note at the momment this is not seen, as game canvas is immediatly hidden
     *  and menu canvas is made visable
     *  
     */
    public void wonGame() {
        game.drawImage(congratsImg , 90, 50, 615, 88);
         if (soundOn && !isPlayed) {
            try {
               isPlayed = true;
               winAudio.play();
            }
            catch (Exception f) {
                f.printStackTrace();
            }
        }
        
        if (won && wonTimer == null) {
            wonTimer = new Timer(wonDelay, this);
            wonTimer.start();
        }
        
    }
    
    public void endGame() {
        if (animationTimer != null) {
            animationTimer.stop();
            animationTimer = null;
        }

        if (wonTimer != null) {
            wonTimer.stop();
            wonTimer = null;
        }
        
        game.clear();     
        game.hide();
        game = null;
        menu.show();
    }
    
    public void checkStatus() {
        pz.check(); // ask pz to check game state
        if (pz.getPeices() == 1) { //if only 1 peice left we must have won
            won = true; //change won state
        }        
    }
    
    /** Called when Mouse button is released i.e. may have just finished a drag and drop of a DNA fragment
     * 
     */
    public void mouseReleased(MouseEvent e)
    {
        int button = -1;
       
        int x = e.getX();
        int y = e.getY();
        
        for (int i = 0; i < numGameButtons; i++) {
                if(gameButtons[i].isPoint(x, y))
                    button = i;
        }

        switch (button) {
        case 0:
            endGame();    
            break;
        case 1:
            if (!demo && !autoMove) {
                pz.hint();
                drawDNA();
            }
            break;
        case 2:
            if (!demo && !autoMove)        
                solveAll();
            break;            
        case 3:
            if(!won) {
                if (soundOn) {
                    soundOn = !soundOn;
                    gameButtons[3].setImage(soundOffImg);
                }
                else {
                    soundOn = !soundOn;
                    gameButtons[3].setImage(soundOnImg);               
                }

                gameButtons[3].drawButton(game);
                game.repaint();
            }
            break;
        default:
            break;
        }

        if (dragging && !autoMove) {
            dragging = false;
            peice = -1;
        }
        if (!demo && !autoMove)
            checkStatus();
        drawDNA(); //update display
    }
    
    public void mouseEntered(MouseEvent e) //needed for MouseListener
    {
    }
    
    public void mouseExited(MouseEvent e)  //needed for MouseListener
    {
    }   
    
    public void componentResized(ComponentEvent e) {
        int oldWidth = game.getWidth();
        int oldHeight = game.getHeight();
        if (e.getComponent().getWidth() < 800 || e.getComponent().getHeight() < 600)
            return;
        game.updateSize();
        int newWidth = game.getWidth();
        int newHeight = game.getHeight();
        int currentX = 0;
        int currentY = 0;
        int currentWidth = 0;
        int currentHeight = 0;
        double widthRatio = (double) newWidth / (double) oldWidth;
        double heightRatio = (double) newHeight / (double) oldHeight;

        settings.bottomBorder = newHeight / 4;
        settings.width = newWidth;
        settings.height = newHeight;
        
        
        for (int i = 1; i <= pz.getPeices(); i++) { //loop thru peices and draw on game canvas
            try {
                currentX = (int) ((double) pz.getFragment(i).getX() * widthRatio);
                currentY = (int) ((double) pz.getFragment(i).getY() * heightRatio);
                
                pz.getFragment(i).setX(currentX); 
                pz.getFragment(i).setY(currentY); 
            }
            catch (Exception ex)
            {
                System.out.println("Error");  //exception catch, shouldnt ever get here
            }
        }
        /*
        for (int i = 0; i < numGameButtons; i++) { //loop thru peices and draw on game canvas
            currentX = (int) ((double) gameButtons[i].getX() * widthRatio);
            currentY = (int) ((double) gameButtons[i].getY() * heightRatio);
            currentWidth = (int) ((double) gameButtons[i].getWidth() * widthRatio);
            currentHeight = (int) ((double) gameButtons[i].getHeight() * heightRatio);            
                
            gameButtons[i].setX(currentX);
            gameButtons[i].setY(currentY);
            gameButtons[i].setWidth(currentWidth);
            gameButtons[i].setHeight(currentHeight);                
        }
        */
        initButtons();
        

        drawDNA();
    }
    
    public void componentMoved(ComponentEvent e) {
    }
    
    public void componentShown(ComponentEvent e) {
    }

    public void componentHidden(ComponentEvent e) {
    }
    
    public void actionPerformed(ActionEvent evt) {
         if (!game.getFrame().isShowing()) {
             endGame();
         }
        
        
         if (demo && won && wonTimer != null && !autoMove) {
            won = false;
            isPlayed = false;
            if (animationTimer != null) {
                animationTimer.stop();
                animationTimer = null;            
            }
            wonTimer.stop();
            wonTimer = null;
            dragging = false;
            peice = -1;
            settings.peices = settings.originalPeices;
            initDNA();
            solveAll();
        }
        
        if (autoMovements > 0 && autoMove) { // if movements are still left to be done
            double fraction = (double) (movements -  autoMovements ) / (double) movements;
            origAutoMove.moveToward(destAutoMove, fraction );
            drawDNA();
            autoMovements--;
        } 
            
        if (autoMovements <= 0 && autoMove) { // if we have done or are past required amount of movements
            checkStatus(); //check status
            dragging = false;
            drawDNA(); //redraw
            
            autoMove = false; //stop automoving
            animationTimer.stop();
            animationTimer = null;

            if (solveAll & pz.getPeices() > 1) //if solving everything and peices left
                solve(); //solve next set of peices

        }
    }
}
