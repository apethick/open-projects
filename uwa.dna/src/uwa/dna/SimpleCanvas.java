package uwa.dna;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.net.URL;
/** SimpleCanvas is the main class used for rendering
 *
 */
public class SimpleCanvas
{
    private JFrame frame;
    private CanvasPane canvas;
    private Graphics2D graphic;
    private Image canvasImage;
    private boolean autoRepaint;
    private int nativeWidth;
    private int nativeHeight;
    private int width;
    private int height;
    private ImageLoader il;
    //static Images for rendering amino acids from a file
    private URL aURL = this.getClass().getResource("img/A.gif"); 
    private URL gURL = this.getClass().getResource("img/G.gif"); 
    private URL tURL = this.getClass().getResource("img/T.gif"); 
    private URL cURL = this.getClass().getResource("img/C.gif");     

    private Image aImg, gImg, tImg, cImg;


    /**
     * Creates and displays a SimpleCanvas of the specified size
     * 
     * @param   title title for the window
     * @param   width the desired width of the SimpleCanvas 
     * @param   height the desired height of the SimpleCanvas
     * 
     */
    public SimpleCanvas(String title, int width, int height) {
        //preparesImages for rendering
        loadImages();
        
        //creates a JFrame and CanvasPane for displaying graphics on
        frame = new JFrame();
        canvas = new CanvasPane();
        frame.setContentPane(canvas);
        //exit when X is clicked
        frame.setDefaultCloseOperation ( JFrame.EXIT_ON_CLOSE );
        frame.setTitle(title);
        
        // Get size of 1st screen        
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice gs = ge.getDefaultScreenDevice();
        GraphicsConfiguration graphicConf = gs.getDefaultConfiguration();
        
        //this code can be use to automatically set canvas to size of users screen
        DisplayMode dm = gs.getDisplayMode();
        nativeWidth = dm.getWidth();
        nativeHeight = dm.getHeight();
        
        canvas.setPreferredSize(new Dimension(width, height));
        frame.pack();

        //set size variables
        Dimension size = canvas.getSize();
        this.width = size.width;
        this.height = size.height;
        //get a link to the  Graphics2D object for drawing
        canvasImage = canvas.createImage(this.width, this.height);
        graphic = (Graphics2D) canvasImage.getGraphics();
        this.clear(); //clear screen
        frame.setVisible(true); //make frame visable
    }
    
    public JFrame getFrame() {
        return frame;
    }
    
    private void loadImages() {
        il = new ImageLoader();
        aImg = il.loadImage(aURL);
        gImg = il.loadImage(gURL);
        tImg = il.loadImage(tURL);
        cImg = il.loadImage(cURL);        
    }

    public void updateSize() {
        Dimension size = canvas.getSize();
        this.width = size.width;
        this.height = size.height;        
        canvasImage = canvas.createImage(this.width, this.height);
        graphic = (Graphics2D) canvasImage.getGraphics();
    }
    
    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
    
    //returns appropriate picture reference
    // I thought it more appropriate to have it here rather than duplicating it in memory evertime
    // a fragment is created
    public Image getImg(char c) {
        switch (c) {
            case 'G':
                return gImg;
            case 'C':
                return cImg;
            case 'T':
                return tImg;
            case 'A':
                return aImg;
            default:
                return null;
        }
    }
    
    /**
     * Creates and displays a SimpleCanvas of size 400x400 with the
     * default title "SimpleCanvas" and with automatic repainting 
     * enabled.
     * 
     * 
     */
    public SimpleCanvas() {
        this("SimpleCanvas", 400, 400);
    }
    
    /** 
     * Draws a circle on the SimpleCanvas
     * 
     * @param   x x-coordinate of the centre of the circle
     * @param   y y-coordinate of the centre of the circle
     * @param   r radius of the circle
     * @param   fill true if circle is to be filled
     *
     */
    public void drawCircle(int x, int y, int r, boolean fill) {
        if (fill)
            graphic.fillOval(x - r, y - r, r * 2, r * 2);
        else
            graphic.drawOval(x - r, y - r, r * 2, r * 2);
    }
    
    //draws a line from x1,y1 to x2,y2
    public void drawLine(int x1, int y1, int x2, int y2) {
        graphic.drawLine(x1, y1, x2, y2);
    }
    
    //draws a square from x1,y1 to x2,y2
    //fills it if fill is true
    public void drawSquare(int x1, int y1, int x2, int y2, boolean fill) {
        if (fill)
            graphic.fillRect(x1, y1, x2 - x1, y2 - y1);
        else {
            graphic.drawLine(x1, y1, x2, y1);
            graphic.drawLine(x2, y1, x2, y2);
            graphic.drawLine(x2, y2, x1, y2);
            graphic.drawLine(x1, y2, x1, y1);
        }
    }    
    
    //clear screen white
    public void clear() {
        Color temp = graphic.getColor();
        graphic.setColor(Color.white);
        graphic.fillRect(0, 0, width, height);
        graphic.setColor(temp);        
    }
    
    //hide window
    public void hide() {
        frame.setVisible(false);
    }
    
    //show window
    public void show() {
        frame.setVisible(true);
    }
    
    //draw image as background
    public void drawBackground(Image img) {
        graphic.drawImage(img , 0, 0, width, height, null);
        canvas.repaint();
//        graphic.drawImage(img , 0, 0, width, height, Color.white, null);

    }

    /** 
     * Changes the colour for subsequent 
     * drawing on this SimpleCanvas.
     * 
     * @param   newColour the new drawing colour
     * 
     */
    public void setForegroundColour(Color newColour) {
        graphic.setColor(newColour);
    }
    
    /**
     * Gets the colour currently used for 
     * drawing on this SimpleCanvas.
     * 
     */
    public Color getForegroundColour() {
        return graphic.getColor();
    }
    
    /**
     * Changes the font for subsequent String
     * drawing on this SimpleCanvas.
     *
     * @param   newFont the new Font
     * 
     */
    public void setFont(Font newFont) {
        graphic.setFont(newFont);
    }
    
    /**
     * Gets the font currently used for
     * String drawing on this Canvas
     */
    public Font getFont() {
        return graphic.getFont();
    }
    
    /**
     * Draws the specified String at the specified
     * location on this SimpleCanvas
     */
    public void drawString(String text, int x, int y) {
        graphic.drawString(text, x, y);
    }
    
    
    /**
     * If this SimpleCanvas does not automatically repaint 
     * after each drawing command, then this method can be
     * used to cause a manual repaint.
     */
    public void repaint() {
        canvas.repaint();
    }
    
    
    /**
     * Causes execution to pause for the specified amount of time.
     * This is usually used to produce animations in an easy
     * manner, by repeatedly drawing, pausing, and then redrawing
     * an object.
     */
    public void wait(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ie) {
            System.out.println("Interruption in SimpleCanvas: "+ie);
        }
        repaint();
    }
    
    //adds mouselistener to canvas
    public void addMouseListener(MouseListener ml) {
        canvas.addMouseListener(ml);
    }
    
    public void addComponentListener(ComponentListener cl) {
        canvas.addComponentListener(cl);
    }

    //adds mousemotionlistener to canvas    
    public void addMouseMotionListener(MouseMotionListener ml) {
        canvas.addMouseMotionListener(ml);
    }    

    //draw image full scale at x,y
    public void drawImage(Image img, int x, int y) {
        graphic.drawImage(img , x, y, null);
    }
    
    //draw image at x,y scaled to width, height
    public void drawImage(Image img, int x, int y, int width, int height) {
        graphic.drawImage(img , x, y, width, height, null);
    }
    
    //sub class to extend JPanel
    //I modified this from the original SimpleCanvas to make it smoother
    class CanvasPane extends JPanel {
        public void update(Graphics g) {
            g.drawImage(canvasImage,0,0,null);
        }

        public void paint(Graphics g) {
            update(g);
        }
    }
   
}


