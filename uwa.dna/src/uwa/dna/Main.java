package uwa.dna;

/**
 * Main Class to Start Application
 * 
 * @author Daniel McGregor
 * @version 1.00
 */
public class Main
{
    private static DNAAssemblyGame dag;

    /**
     * Constructor for objects of class Main
     */
    public static void main(String [ ] args)
    {
  
        dag = new DNAAssemblyGame();
    }

}
