package uwa.dna;
import java.util.Random;
import java.applet.*;
import java.net.*;

public class DNA
{
    // instance variables
    //HIV Gene :D
    private String origDNA;//
    private String sDNA;     //used to hold a substring of origDNA
    private DNAFragment[] DNAFragments; // holds an array of fragments
    private Random generator = new Random(); //random num generator
    private Settings settings; 
    private int levelNumber;
   
    private int hintPeice = -1;
   
    
    
    public DNA(Settings s) {
        settings = s; //make a copy of settings
        int start = 0; // initalize start pos to 0
        levelNumber = Math.abs(generator.nextInt()) % settings.levelGenerator.getNumberOfLevels();
        origDNA = settings.levelGenerator.getLevel(levelNumber);
        if (settings.size + 1 < origDNA.length()) // check if requested size is not too big
            start = (Math.abs(generator.nextInt()) % (origDNA.length() - settings.size)); //find a random position in origDNA that is
            //at least settings.size away from the end
        else {
            settings.size = origDNA.length(); //use origDNA length instead (unlikely)
            start = 0;
        }
        sDNA = origDNA.substring(start, start + settings.size); // extract a portion of origDNA to use for game
        createFragments(); // create fragments from extract
    }

    public String getLevelName() {
        return settings.levelGenerator.getTitle(levelNumber);
    }
    
    public void hint() {
        if (hintPeice > -1) {
            DNAFragments[hintPeice - 1].removeHint();
            DNAFragments[hintPeice].removeHint();
        }
        hintPeice = Math.abs(generator.nextInt()) % (settings.peices - 1) + 1;
        DNAFragments[hintPeice - 1].addHint();
        DNAFragments[hintPeice].addHint();
    }
    
    /** Used to merge DNAFragments a & b
     * This method effectively appends b onto end of a with out the overlap and then deletes b
     */
    public void merge(DNAFragment a, DNAFragment b) {
            if (hintPeice > -1) {
         
                DNAFragments[hintPeice - 1].removeHint();
                DNAFragments[hintPeice].removeHint();
                hintPeice = -1;
            }
            int aIndex, bIndex;
            aIndex = a.getSequence() - 1; //convert Sequence number to index
            bIndex = b.getSequence() - 1;
            DNAFragments[aIndex] = a.merge(b); //repalce fragment a with merged a & b fragments
            for (int i = bIndex; i < settings.peices - 1; i++) { //loop thru remaining peices
                DNAFragments[i] = DNAFragments[i + 1]; //move next peice to current position
                DNAFragments[i].reduceSequence(); //lowers fragments internal sequence count by 1
            }
            settings.peices--; //lower peices count
    }
    /** creates fragments from sDNA string
     * 
     */
    public void createFragments()
    {
        int s = 0; //used to represetn start of fragment
        int f = 0; //used to represetn end of fragment
        DNAFragments = new DNAFragment[settings.peices]; //creates an array of DNAFragments
        for (int i = 0; i < settings.peices; i++) { //loop thru all peices
            s = f - settings.overlap; //start = end of last peice - overlap
            f = (sDNA.length() / settings.peices) * (i + 1) + settings.overlap; //finish = length of sDNA / number of peices * sequence + overlap
            
            if (s < 0) //for i = 0 s would be -overlap, this sets it to 0
                s = 0;
            if (f > sDNA.length()) //final f may be over the length of sDNA if length of sDNA / number of peices had a remainder
                f = sDNA.length();              //this sets it to final position of sDNA
        DNAFragments[i] = new DNAFragment(0, 0, sDNA.substring(s, f), i + 1, settings); //create a new fragment a (0, 0) with a portion of sDNA
        // and a sequence number of i + 1
        }
    }
    
    
    /** returns number of peices
     * 
     */
    public int getPeices() {
        return settings.peices;
    }
    
    /** returns the first fragment that x, y falls in
     * 
     */
    public int isPeice(int x, int y) {
        int peice = -1;
        for (int i = 0; i < settings.peices; i ++) { //loop thru peices
            if (DNAFragments[i].isPoint(x, y)) //if point (x,y) is in a fragment
                peice = DNAFragments[i].getSequence();    
        }

        return  peice; //return fragments sequence
    }
    
    /** returns fragment
     * 
     */
    public DNAFragment getFragment(int fragment) throws Exception {
        if(fragment <= settings.peices && fragment > 0) //checks for valid fragment
            return DNAFragments[--fragment]; //returns fragment
        else
            throw new Exception(); //exception if invalid fragment requested (shouldnt happen)
    }

    public void check() {
        DNAFragment iFragment, jFragment; //two temporary placeholders for fragments
        for (int i = 0; i < settings.peices; i ++) { //loop thru all fragments
            iFragment = DNAFragments[i];
            for (int j = 0; j < settings.peices; j++) { //loop thru all fragments again
                if (i != j) { // if we arent comparing a fragment to itself
                    jFragment = DNAFragments[j];
                    if (iFragment.isLeftOf(jFragment) && iFragment.before(jFragment)) { 
                        // if fragment I is left of fragment J (That is the right half of fragment I
                        // is overlaping part of the left half of fragment J)
                        // and if I is immediatley before J
                        merge(iFragment, jFragment); // merge the two 
                    }
                }
            }
        }
    }
    
}
