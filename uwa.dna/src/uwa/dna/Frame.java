package uwa.dna;
import java.awt.*;
import java.net.URL;
import java.applet.*;

public class Frame {
    
    public int timeLength;
    private ImageLoader il = new ImageLoader();
    public Image frameImage;
    public AudioClip helpAudio = null;
    public int width;
    public int height;
    public String text;
    public String text2;
    private int nText;
    private int text1h = 80;
    private int text2h = 535;
    private int text1v = 80;
    private int text2v = 565;
    public SimpleCanvas sc;     
  
    private Font helpTextFont = new Font("Comic Sans", Font.BOLD, 16);
    
    public Frame(SimpleCanvas sc, String imagePath, String soundPath, String text, int timeLength, int width, int height) {
     this.sc = sc;
     frameImage = il.loadImage(this.getClass().getResource(imagePath));
     if (soundPath != "")
        helpAudio = Applet.newAudioClip(this.getClass().getResource(soundPath));
     this.text = text;                                      //This is the Helps text subtitle to the Sound URL
     this.timeLength = timeLength;                          //This is the time length in msec
     nText = 1;
    }
    
     public Frame(SimpleCanvas sc, String imagePath, String soundPath, String text,  String text2, int timeLength, int width, int height) {
     this.sc = sc;
     frameImage = il.loadImage(this.getClass().getResource(imagePath));
     if (soundPath != "")
     helpAudio = Applet.newAudioClip(this.getClass().getResource(soundPath));
     this.text = text;                                      //This is the Helps text subtitle to the Sound URL
     this.text2 = text2;
     this.timeLength = timeLength;                          //This is the time length in msec
     nText = 2;    
    }

    private void calcSize() {
        width = sc.getWidth();
        height = sc.getHeight();
        text1h = (int) ((double) width * 0.02);
        text2h = (int) ((double) width * 0.02);
        text1v = (int) ((double) height * 0.9);
        text2v = text1v + 30;
    }
    
    public void displayFrame() {
        displayFrame(true);
    }
    
    public void displayFrame(boolean playSound) {
        calcSize();
        sc.clear();
        sc.drawImage(frameImage, 0, 0, width, (int) (height/1.24));
        sc.setFont(helpTextFont);
        sc.drawString(text, text1h, text1v);
        if (playSound && helpAudio != null)
            helpAudio.play();
        if(nText == 2)
            sc.drawString(text2, text2h, text2v);
        
        sc.repaint();
    }    
}
