package uwa.dna;
/**
 * A Class for holding the configuration of a game
 * Needs to be extended so it can read/save from/to a configuration file, and do some automated checking
 * @author Daniel McGregor
 * @version 0.9
 */
public class Settings
{
    private static final String DEFAULT_DESC            = "EASY";
    private static final int DEFAULT_HEIGHT             = 600;
    private static final int DEFAULT_WIDTH              = 800;
    private static final int DEFAULT_PEICES             = 6;
    private static final int DEFAULT_OVERLAP            = 4;
    private static final int DEFAULT_SIZE               = 40;
    private static final int DEFAULT_CELL_SIZE_H        = 29;
    private static final int DEFAULT_CELL_SIZE_V        = 51;
    private static final int DEFAULT_MAXIMUM_LENGTH     = 12;    
    private static final int DEFAULT_MUTATION_RATE      = 0;

    // static objects used to hold setting for EASY, MEDIUM and HARD
    public static final Settings EASY          = new Settings("EASY", DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_PEICES, DEFAULT_OVERLAP, DEFAULT_SIZE, DEFAULT_MUTATION_RATE);
    public static final Settings MEDIUM        = new Settings("MEDIUM", DEFAULT_WIDTH, DEFAULT_HEIGHT, 12, 3, 70, 5);    
    public static final Settings HARD          = new Settings("HARD", DEFAULT_WIDTH, DEFAULT_HEIGHT, 20, 2, 100 , 10);

    public Level levelGenerator = new Level();
    public String description;
    public int width;
    public int height;
    public int peices, originalPeices;
    public int overlap;
    public int size;
    public int cellSizeH;
    public int cellSizeV;
    public int maximumFragmentLength;
    public int bottomBorder = height / 4;
    public int mutationRate;
    public int maxMutationRate = 100;    
    public int pxOverlap;

    public Settings(Settings s) {
        this.width = s.width;
        this.height = s.height;
        this.peices = s.peices;
        this.originalPeices = this.peices;
        this.overlap = s.overlap;
        this.size = s.size;
        this.cellSizeH = s.cellSizeH;
        this.cellSizeV = s.cellSizeV;
        this.maximumFragmentLength = s.maximumFragmentLength;
        this.bottomBorder = height / 5;
        this.mutationRate = s.mutationRate;
        this.pxOverlap = s.overlap * s.cellSizeH;
        this.description = s.description;
    }
    
    public Settings(String desc, int width, int height, int peices, int overlap, int size, int mutationRate)
    {
        this.description = desc;
        this.width = width;
        this.height = height;
        this.peices = peices;
        this.originalPeices = this.peices;        
        this.overlap = overlap;
        this.size = size;
        this.mutationRate = mutationRate;
        cellSizeH = DEFAULT_CELL_SIZE_H;
        cellSizeV = DEFAULT_CELL_SIZE_V;
        maximumFragmentLength = DEFAULT_MAXIMUM_LENGTH;
        bottomBorder = height / 4;
        this.pxOverlap = overlap * cellSizeH;        
    }
    
    public Settings()
    {
        this(DEFAULT_DESC, DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_PEICES, DEFAULT_OVERLAP, DEFAULT_SIZE, DEFAULT_MUTATION_RATE);
    }

}
