package uwa.dna;
import java.awt.*;
import java.util.Random;
/**
 * Class for DNAFragments
 * 
 * @author Daniel McGregor
 * @version 0.1
 */
public class DNAFragment
{
    private int x;
    private int y;
    private int width, height;
    private String ActualDNAFragment; //actual true representation of DNA fragment
    private String DNAFragment;      // fragment that will be displayed - could have errors (use this to introduce extra difficutly)
    private int sequence;
    private Settings settings;
    private boolean hintStatus = false;
    public ImageLoader il = new ImageLoader();
    private Image  hintBackImageL = il.loadImage(this.getClass().getResource("img/backing-l.gif"));
    private Image  hintBackImageM = il.loadImage(this.getClass().getResource("img/backing-m.gif"));
    private Image  hintBackImageR = il.loadImage(this.getClass().getResource("img/backing-r.gif"));    
    
    /**
     * Constructor for objects of class DNAFragment
     */
    public DNAFragment(int xPos, int yPos, String f, int seq, Settings s)
    {    
        x = xPos;
        y = yPos;
        settings = s;
        ActualDNAFragment = new String(f); //actual fragment
        if (f.length() > settings.maximumFragmentLength) {// if length of fragment is greater than maximum allowed fragment length
            //this line below basically removes the middle 1/3 of the fragment and replaces it with a - to indicate a known, but not shown section
            DNAFragment = mutate(f.substring(0, settings.maximumFragmentLength / 3)) + "-" + mutate(f.substring(f.length() - settings.maximumFragmentLength / 3, f.length()));
        }
        else {
            DNAFragment = mutate(f);
        }
        sequence = seq;
        width = settings.cellSizeH * DNAFragment.length(); // width of peice is number of bases times pixel width of a single Amino Acid
        height = settings.cellSizeV;
    }

    public DNAFragment(int xPos, int yPos, String f, int seq, Settings s, boolean doMutate)
    {    
        x = xPos;
        y = yPos;
        settings = s;
        ActualDNAFragment = new String(f); //actual fragment
        if (f.length() > settings.maximumFragmentLength) {// if length of fragment is greater than maximum allowed fragment length
            //this line below basically removes the middle 1/3 of the fragment and replaces it with a - to indicate a known, but not shown section
            if (doMutate)
                DNAFragment = mutate(f.substring(0, settings.maximumFragmentLength / 3)) + "-" + mutate(f.substring(f.length() - settings.maximumFragmentLength / 3, f.length()));
            else
                DNAFragment = f.substring(0, settings.maximumFragmentLength / 3) + "-" + f.substring(f.length() - settings.maximumFragmentLength / 3, f.length());
        }
        else {
            if (doMutate)
                DNAFragment = mutate(f);
            else
                DNAFragment = f;
        }
        sequence = seq;
        width = settings.cellSizeH * DNAFragment.length(); // width of peice is number of bases times pixel width of a single Amino Acid
        height = settings.cellSizeV;
    }    
    
    public String mutate(String s) {
        Random r = new Random();
        String result = "";
        for (int i = 0; i < s.length(); i++) {
            if ((Math.abs(r.nextInt()) % settings.maxMutationRate) < settings.mutationRate) {
                switch (Math.abs(r.nextInt()) % 4) {
                case 0:
                    result = result + "A";
                    break;
                case 1:
                    result = result + "G";
                    break;
                case 2:
                    result = result + "T";
                    break;
                case 3:
                    result = result + "C";
                    break;
                }

            }
            else
                result = result + s.substring(i, i + 1);
        }
        return result;
    }
    
    /** returns x pos
     * 
     */
    public int getX() {
        return x;
    }

    /** returns left side (for readability)
     * 
     */
    public int getLeft() {
        return x;
    }

    /** returns right side (for readability)
     * 
     */
    public int getRight() {
        return x + width;
    }

    /** returns bottom
     * 
     */
    public int getBottom() {
        return y;
    }

    /** returns top
     * 
     */
    public int getTop() {
        return y + height;
    }

    /** returns x pos
     * 
     */
    public int getY() {
        return y;
    }
    
    /** returns width
     * 
     */
    public int getWidth() {
        return width;
    }

    /** returns height
     * 
     */
    public int getHeight() {
        return height;
    }
    
    
        /** set x pos
     * 
     */
    public void forceX(int newX) {
        x = newX;
    }
    
    /** set y pos
     * 
     */
    public void forceY(int newY) {
        y = newY;
    }
    
    /** set x pos
     * 
     */
    public boolean setX(int newX) {
       if (newX + (DNAFragment.length() * settings.cellSizeH) < settings.width && newX > 0) {
           x = newX;
           return true;
        }
        else return false;
    }
    
    /** set y pos
     * 
     */
    public boolean setY(int newY) {
        if (newY + settings.cellSizeV < settings.height - settings.bottomBorder && newY > 0) {
            y = newY;
            return true;
        }
        else return false;
    }


    /** reduce sequnce number by 1, used when merging
     * 
     */
    public void reduceSequence() {
        sequence--;
    }
    
    /** returns actual String of DNA Fragment
     * 
     */
    public String getDNAFragment() {
        return ActualDNAFragment;
    }

    /** get sequnce number
     * 
     */
    public int getSequence() {
        return sequence;
    }    
    
    public void removeHint() {
        hintStatus = false;
    }
    
    public void addHint() {
        hintStatus = true;
    }
    
    /** draws fragment on SimpleCanvas sc
     * 
     */
    public void drawOnCanvas(SimpleCanvas sc) {
            char c;
            int border = 6;
            if (hintStatus) {
               // sc.setForegroundColour(Color.yellow);
              //  sc.drawSquare(x - border, y - border, x + width + border, y + height + border, true); 

//                sc.drawImage(hintBackImage, x - border, y - border, width + border * 3, height + border * 3);
                sc.drawImage(hintBackImageM, x, y - border, width, height + border * 2);
                sc.drawImage(hintBackImageL, x - border * 2, y - border, border * 2, height + border * 2);
                sc.drawImage(hintBackImageR, x + width, y - border, border * 2, height + border * 2);                
            }
            for (int i = 0; i < DNAFragment.length(); i ++) { //loop thru all of fragment
                c = DNAFragment.charAt(i); 
                drawAmino(c, i, sc);//draw amino acid c at position i on canvas sc
            }
    }
    
    /**
     * returns true if peiceX, peiceY is in the fragment
     */
    public boolean isPoint(int peiceX, int peiceY) {
        return (isBetween(x, peiceX, x + width) && isBetween(y, peiceY, y + height));       
    }

    /**
     * returns true if peiceX, peiceY is in the left half of the fragment
     */
    public boolean isPointLeft(int peiceX, int peiceY) {
        return (isBetween(x, peiceX, x + (width / 2)) && isBetween(y, peiceY, y + height));       
    }

    /**
     * returns true if peiceX, peiceY is in the right half of the fragment
     */
    
    public boolean isPointRight(int peiceX, int peiceY) {
        return (isBetween(x + (width / 2), peiceX, x + width) && isBetween(y, peiceY, y + height));       
    }

    /**
     * returns true if this fragment is right of fragment d
     * that is the left half of this fragment overlaps with the right half of fragment d
     * checks by looking at bottom right and top right points of fragment d
     */
    public boolean isRightOf(DNAFragment d) {
        return isPointLeft(d.getRight(), d.getBottom()) || isPointLeft(d.getRight(), d.getTop());
    }

    /**
     * returns true if this fragment is left of fragment d
     * that is the right half of this fragment overlaps with the left half of fragment d
     * checks by looking at bottom left and top left points of fragment d
     */   
    public boolean isLeftOf(DNAFragment d) {
        return isPointRight(d.getLeft(), d.getBottom()) || isPointRight(d.getLeft(), d.getTop());
    }
    
    /** returns true if one of the corners of d is somewhere in this fragment
     * 
     */
    public boolean overlaps(DNAFragment d) {
        int x1, x2, y1, y2;
        x1 = d.getX();
        x2 = x1 + d.getWidth();
        y1 = d.getY();
        y2 = y1 + d.getHeight();
        return isPoint(x1, y1) || isPoint(x1, y2) || isPoint(x2, y1) || isPoint(x2, y2);
    }
    
    /** string representation of fragment
     * 
     */
    public String toString() {
        return "Actual Fragment " + getSequence() + ": " + getDNAFragment() + "\n" + "Shown Fragment: " + DNAFragment;
    }

    /** true if b is between a and c
     * 
     */
    private boolean isBetween(int a, int b, int c) {
        return ((a >= b && b >= c) || (c >= b && b >= a));
    }    
    
    /** returns a new fragment that is at the same position ast his fragment
     * but contains other fragment (without the overlap)
     * this does no checking if these two fragments are meant to joing, it leaves the checking to the DNA collection object
     * Also Merges without remutating the fragment
     */
    public DNAFragment merge(DNAFragment d) {
        return new DNAFragment(getX(), getY(), ActualDNAFragment + d.getDNAFragment().substring(settings.overlap, d.getDNAFragment().length()), getSequence(), this.settings, false);
    }
    
    /** returns true if this fragment is the fragment before d
     * 
     */
    public boolean before(DNAFragment d) {
        return (sequence + 1) == d.getSequence();
    }
    
     
    /** draws aminoacid c at index i on canvas sc
     * 
     */
    public void drawAmino(char c, int i, SimpleCanvas sc) {
       Image img = sc.getImg(c); //get reference to image for amino acid c
       
       if (c == '-') {
            sc.setForegroundColour(Color.black); //draw a black connecting rod if not an amino acid
            sc.drawSquare(x + i * settings.cellSizeH, y + settings.cellSizeV / 4, x + (i + 1) * settings.cellSizeH, y + settings.cellSizeV / 4 * 3, true);
       }
       else {
            sc.drawImage(img, x + i * settings.cellSizeH, y); //draw img
       }
    }
    
    /**
     * Moves Fragment toward another fragment by specified fraction
     */
    public void moveToward(DNAFragment dest, double fraction) {
        int xOrig = 0;
        int yOrig = 0;
        int xDest = 0;
        int yDest = 0;
        int xMove = 0;
        int yMove = 0; 
        xDest = dest.getX() + dest.getWidth() - settings.pxOverlap;
        yDest = dest.getY();
        xOrig = this.getX();
        yOrig = this.getY();
        xMove = xDest - xOrig;
        yMove = yDest - yOrig;
        xMove = (int) ((double) xMove * fraction);
        yMove = (int) ((double) yMove * fraction);      
        x = x + xMove;
        y = y + yMove;
    }
}
