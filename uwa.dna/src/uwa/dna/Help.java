package uwa.dna;
import java.net.URL;
import java.awt.*;
import java.applet.*;
import javax.swing.JFrame;
import java.awt.event.ComponentListener;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
/**
 * Frame
 * Used to house help and play information
 * 
 * @author Andrew Pethick, Modified Daniel McGregor
 * @version 0.5
 * @date 08/05/2007
 */

    


public class Help implements MouseListener, ComponentListener {

    SimpleCanvas sc;
    private int nFrames = 6;    
    private ImageLoader il = new ImageLoader(); 
    private Frame[] helpFrames;
    private int currentFrame;
    private int frameHeight = 600;
    private int frameWidth = 800;
    private int animationDelay = 0;
    private MenuButton[] buttons;
    private int numberOfButtons = 3;
    private int buttonsRightBorder = 120;
    private int buttonsBottomBorder = 100;
    private int buttonsHeight = 50;
    private boolean exit = false;
    
    private URL forwardURL = this.getClass().getResource("img/helpforward.gif");
    private URL backURL = this.getClass().getResource("img/helpback.gif");
     private URL exitURL = this.getClass().getResource("img/helpExit.gif");
    
    private Image forwardImg, backImg,  helpExitImg;
        
    public Help(SimpleCanvas sc)    {
        this.sc = sc;
        sc.getFrame().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);        
        loadImages();
        initFrames();
        initButtons();
        cycleThroughFrames();
        sc.addComponentListener(this);
        sc.addMouseListener(this);
    }
    
    private void loadImages() {
       backImg = il.loadImage(backURL);
       forwardImg = il.loadImage(forwardURL);
       helpExitImg = il.loadImage(exitURL);
    }
    
    public Help() {
        this(new SimpleCanvas("DNA Assembly Game Help", 800, 600));
    }
    
    private void initFrames() {
        buttons = new MenuButton[numberOfButtons];
        String soundFile = "";
        helpFrames = new Frame [nFrames];    
        helpFrames[0] = new Frame(sc, "img/helpSlide1.jpg", soundFile, "A single strand of DNA is sequenced into a number of fragments.", "The purpose of this game is to join them back up again.", animationDelay, frameHeight, frameWidth);
        helpFrames[1] = new Frame(sc, "img/helpSlide2.jpg", soundFile, "We can perform this by finding overlapping patterns in the DNA fragments.", animationDelay, frameHeight, frameWidth);
        helpFrames[2] = new Frame(sc, "img/helpSlide3.jpg", soundFile, "The sequence is built by dragging these overlapping patterns", "over one another.", animationDelay, frameHeight, frameWidth);
        helpFrames[3] = new Frame(sc, "img/helpSlide4.jpg", soundFile, "The amount of overlap between fragments can be seen at the bottom left", "corner of the screen.", animationDelay, frameHeight, frameWidth);
        helpFrames[4] = new Frame(sc, "img/helpSlide5.jpg", soundFile, "In reality DNA sequencing is not 100% correct.", "Some fragments that should overlap may appear to be different.", animationDelay, frameHeight, frameWidth);
        helpFrames[5] = new Frame(sc, "img/helpSlide6.jpg", soundFile, "If you get stuck the Hint button will highlight two fragments which overlap.", "The Solve button can be used to view the final solution.", animationDelay, frameHeight, frameWidth);
    }
    
    private void initButtons() {
        buttons[0] = new MenuButton(0, "Forward", frameWidth - buttonsRightBorder, frameHeight - buttonsBottomBorder, 95, 43, forwardImg);
        buttons[1] = new MenuButton(1, "Back", frameWidth - buttonsRightBorder, frameHeight - buttonsBottomBorder + buttonsHeight, 95, 43, backImg);      
        buttons[2] = new MenuButton(2, "Exit", frameWidth - buttonsRightBorder, frameHeight - buttonsBottomBorder, 95, 43, helpExitImg);
    }
   
    public void cycleThroughFrames() {
        currentFrame = 0;
        drawFrame();     
    }

    public void drawFrame() {
        helpFrames[currentFrame].displayFrame();

        if (currentFrame != 0)
            buttons[1].drawButton(sc);
        
        if (currentFrame != nFrames - 1)
            buttons[0].drawButton(sc);
        else buttons[2].drawButton(sc);
         
        
            
        sc.repaint();
    }
    
    public void forwardFrame() {
            currentFrame = currentFrame + 1;
            if (currentFrame == nFrames)
              sc.getFrame().dispose();

            drawFrame();
    }
    
    public void backFrame() {
            currentFrame = currentFrame - 1;
            if (currentFrame < 0)
                currentFrame = 0;
            drawFrame();
    }
    
    public void remove() {
        if (!sc.getFrame().isShowing()) {
            for (int i = 0; i < nFrames; i++)
                helpFrames[i] = null;
            sc = null;
            return;
        }
    }
    
    public void mouseReleased(MouseEvent e)
    {
        int button = -1;
        int x = e.getX();
        int y = e.getY();
        
        for (int i = 0; i < numberOfButtons; i++) { //check to see if a button was clicked
            if(buttons[i].isPoint(x, y)) {
                button = i;
                break;
            }        
        }

            switch (button) {
            case 0:
                forwardFrame();
                break;
            case 1:
                backFrame();
                break;
            default:
            }
    }
    
    public void componentResized(ComponentEvent e) {
        if (e.getComponent().getWidth() < 800 || e.getComponent().getHeight() < 600)
            return;        
        sc.updateSize();
        frameWidth = sc.getWidth();
        frameHeight = sc.getHeight();
        initButtons();        
        drawFrame();
    }
    
    public void componentMoved(ComponentEvent e) {
    }
    
    public void componentShown(ComponentEvent e) {
    }

    public void componentHidden(ComponentEvent e) {
    }     
    
    public void mouseEntered(MouseEvent e) //needed for MouseListener
    {
    }
    
    public void mouseExited(MouseEvent e)  //needed for MouseListener
    {
    }  

    public void mouseClicked(MouseEvent e) //needed for MouseListener
    {
    }

    public void mousePressed(MouseEvent e)
    {   
    }   
     
}
