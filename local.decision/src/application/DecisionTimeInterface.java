package application;

import java.util.ArrayList;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class DecisionTimeInterface extends Stage{
	private BorderPane main;
	private Scene scene;
	private DecisionTime dt;

	public DecisionTimeInterface (DecisionTime dt) {
		this.dt = dt;
		this.setWidth(800);
		this.setHeight(600);
		this.main = new BorderPane();
		this.scene = new Scene(main);
		this.setScene(scene);
		this.show();
		this.setOnCloseRequest(new EventHandler<WindowEvent>() {
		    @Override
		    public void handle(WindowEvent event) {
		        try {
					dt.stop();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});

		startRound(dt.current_iteration);
	}

	private void startRound(int current_iteration) {
		if(current_iteration > dt.max_iterations) showResults();
		else {
			Round r = new Round(dt);
			show(r,0);
			dt.current_iteration++;
		}
	}

	

	private void show(Round r, int i) {

		if(i >= r.comp.size()) {
			finishRound();
		} else {

			HBox comparisonPane = new HBox();

			Label vs = new Label("V.S.");
			
			Item [] items = r.comp.get(i);
			Button c1 = new Button(items[0].getName());
			Button c2 = new Button(items[1].getName());
			comparisonPane.getChildren().addAll(c1,vs,c2);
			c1.setPrefWidth(300);
			c2.setPrefWidth(300);
			c1.setPrefHeight(100);
			c2.setPrefHeight(100);
			vs.setPrefHeight(100);
			
			c1.maxWidth(300);
			c2.maxWidth(300);
			HBox.setHgrow(c1, Priority.ALWAYS);
			HBox.setHgrow(c2, Priority.ALWAYS);
			c1.setMaxWidth(Double.MAX_VALUE);
			c2.setMaxWidth(Double.MAX_VALUE);
//			c1.setMaxWidth(Double.MAX_VALUE);
//			c2.setMaxWidth(Double.MAX_VALUE);
			main.setBottom(new Text(i + "/" + (r.comp.size())));
			c1.setOnAction(new EventHandler<ActionEvent>() {
				
				@Override
				public void handle(ActionEvent event) {
					dt.algorithm.updateRatings(items[0], items[1]);
					show(r,i+1);
				}
			});
			c2.setOnAction(new EventHandler<ActionEvent>() {
				
				@Override
				public void handle(ActionEvent event) {
					dt.algorithm.updateRatings(items[1], items[0]);
					show(r,i+1);
				}
			});
			main.setCenter(comparisonPane);
		}
	}

	private void finishRound() {
		main.setBottom(new Text(""));
		Button next = new Button(dt.current_iteration > dt.max_iterations ? "SHOW RESULTS" : "NEXT ROUND " + dt.current_iteration + "/" + dt.max_iterations);
		next.setPrefWidth(600);
		next.setPrefHeight(400);
		next.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				startRound(dt.current_iteration);
			}
		});
		main.setCenter(next);
	}

	private void showResults() {
		VBox results = new VBox();
		ArrayList<Item> items = dt.manager.getAllRankedItems();
		int rank = 1;
		for(Item i : items) {
			results.getChildren().add(new Text(rank++ + " " + i.getName() + ": " + i.getRating()));
		}
		main.setCenter(results);
	}
}
