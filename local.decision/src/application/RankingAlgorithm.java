package application;

public class RankingAlgorithm {


	   public RankingAlgorithm()  {
	    
	   }
	 
	   /*
	    * returns the expected scores using the formula : Ea = 1/(1+10^(Ra-Rb)/400)
	    */
	   public static double getExpectedScore(Item person, Item competitor)
	   {
	       return 1/(1+Math.pow(10,(double)(person.getRating()-competitor.getRating())));
	   }

	   /*
	    * updating the ratings.
	    */
	   public void updateRatings(Item winner, Item loser)
	   {
		
	       double expectedScoreOfWinner = getExpectedScore(winner,loser);
	       double expectedScoreOfLoser  = getExpectedScore(loser,winner);    
	       /*
	        * Formula : new rating = K (ActualRating-ExpectedRating)
	        * ActualRating = 1 if the person wins
	        * ExpectedRating = 0 if the person loses
	        * Here K = 10
	        */
	       winner.saveValue(winner.getRating() + (int)(10*(1-expectedScoreOfWinner)));
	       loser.saveValue(loser.getRating() + (int)(10*(0-expectedScoreOfLoser)));    
	   }

	 

	  

}
