package application;
	
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;


public class DecisionTime extends Application {
	public static final int MAX_SIZE = 2000;
	public int max_iterations = 5;
	public int current_iteration = 1;
	public ListManager manager;
	
	public final RankingAlgorithm algorithm = new RankingAlgorithm();

	@Override
	public void start(Stage primaryStage) {
		try {
			
			this.manager = new ListManager(this);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
