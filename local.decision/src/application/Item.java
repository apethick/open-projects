package application;

import java.util.Properties;

public class Item {
	private final String name;
	private int value;
	private boolean isBlacklisted;
	private ListManager manager;
	
	public Item(String name, int value, boolean blacklisted, ListManager manager) {
		this.name = name;
		this.value = value;
		this.isBlacklisted = blacklisted;
		this.manager = manager;
	}
	
	public void save() {
		manager.props.put(name + ".NAME", name);
		manager.props.put(name + ".VALUE", "" + value);
		manager.props.put(name + ".BLACK", isBlacklisted ? "TRUE" : "FALSE");
		manager.save();
	}

	public int getRating() {
		return value;
	}
	public void saveValue(int value) {
		this.value = value;
		save();
	}

	public String getName() {
		return name;
	}
}
