package application;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class Round {
	private DecisionTime dt;
	public final ArrayList<Item[]> comp;

	public Round(DecisionTime dt) {
		this.dt = dt;
		ArrayList<String> list = new ArrayList<>(dt.manager.list);
		shuffleList(list);
		ArrayList<Item[]> comp = new ArrayList<Item[]>();
		Item [] l = new Item[2];

		for(String s : list) {			
			Item i = dt.manager.getItem(s);
			if(l[0] == null) {
				l[0] = i;
			} else if(l[1] == null) {
				l[1] = i;
			} else {
				comp.add(new Item[]{l[0],l[1]});
				l = new Item[2];
				l[0] = i;
			}
		}
		if(l[0] == null) {
			
		} else if(l[1] == null) {
			System.out.println("IGNORING " + l[0].getName());
		} else {
			comp.add(l);
		}
		this.comp = comp;
	}
	  public static void shuffleList(ArrayList<String> a) {
		    int n = a.size();
		    Random random = new Random();
		    random.nextInt();
		    for (int i = 0; i < n; i++) {
		      int change = i + random.nextInt(n - i);
		      swap(a, i, change);
		    }
		  }

		  private static void swap(ArrayList<String> a, int i, int change) {
			  String helper = a.get(i);
		    a.set(i, a.get(change));
		    a.set(change, helper);
		  }

	
}
