package application;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Properties;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class ListManager {
	
	File generalList = new File("Resources\\Lists\\");
	File propertiesFile = new File("Resources\\Properties\\props.properties");
	protected File file;
	protected ResourceBundle bundle;
	public Properties props;
	private boolean fullReset = true;
	public ArrayList<String> list = new ArrayList<String>();
	private DecisionTime main;
	public ListManager(DecisionTime main) {
		this.main = main;
		askForList();
		
	}

	private void askForList() {
		Stage stage = new Stage();
		VBox box = new VBox();
		ScrollPane sp = new ScrollPane();
		sp.setContent(box);
		BorderPane main = new BorderPane();
		main.setCenter(sp);
		main.setTop(new Text("Select your List"));
		stage.setScene(new Scene(main));
		for(File f : generalList.listFiles()) {
			if(f.getName().toLowerCase().endsWith(".txt")) {
				Button b = new Button(f.getName().split("\\.")[0]);
				b.setOnAction(new EventHandler<ActionEvent>() {
					
					@Override
					public void handle(ActionEvent event) {
						ListManager.this.file = f;
						initialiseList();	
						DecisionTimeInterface d = new DecisionTimeInterface(ListManager.this.main);
					}

				
				});
				box.getChildren().add(b);				
			}
		}
		stage.setWidth(400);
		stage.setHeight(900);
		stage.show();		
	}

	private void initialiseList() {
		resetList();
		try {
			BufferedReader in = new BufferedReader(new FileReader(file));
			String line = "";
			while((line = in.readLine())!= null) {
				if(line.length() > 1) {
					addItem(new String(line));
				}
			}
		} catch (Exception e) {		
			e.printStackTrace();
		}
		
	}
	private void resetList() {
		list = new ArrayList<String>();
		if(fullReset ) {
			if(propertiesFile.exists()) propertiesFile.delete();
			try {
				propertiesFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		try {
			props = new Properties();
			BufferedInputStream in = new BufferedInputStream(new FileInputStream(propertiesFile));
			if (!propertiesFile.exists()) {
				propertiesFile.createNewFile();
			} else {
				props.load(in);
			}
	
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	private void addItem(String s) {
		props.put(s + ".NAME", s);
		props.put(s + ".VALUE", "" + DecisionTime.MAX_SIZE);
		props.put(s + ".BLACK", "FALSE");
		list.add(s);
	}

	public Item getItem(String s) {
		int value = Integer.valueOf(props.get(s + ".VALUE").toString());
		boolean isBlacklisted = props.get(s + ".BLACK").toString().toUpperCase().contains("TRUE");
		Item i = new Item(s, value, isBlacklisted, this);
		return i;
	}

	public void save() {
		try {
		BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(propertiesFile));
		props.store(out, "Saved!");
		out.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Item> getAllRankedItems() {
		ArrayList<Item> items = new ArrayList<Item>();
		for(String s : this.list) {
			items.add(getItem(s));
		}
		Collections.sort(items,new Comparator<Item>() {

			@Override
			public int compare(Item o1, Item o2) {
				return new Integer(o2.getRating()).compareTo(new Integer(o1.getRating()));
			}
		});
		return items;
	}
 	
}
