package local.maze;

import java.util.*;

public class PathStack extends Vector
{
    public PathStack ()
    {
    }

    public void push (MazeSquare m)
    {
	//System.out.println ("PUSH: "+m.x+", "+m.y);
	addElement (m);
    }

    public MazeSquare pop ()
    {
	if (isEmpty ())
	    return null;
	MazeSquare retval = (MazeSquare) lastElement ();
	removeElement (retval);
	//System.out.println ("POP: "+retval.x+", "+retval.y);
	return retval;
    }

    public MazeSquare get (int index)
    {
	return (MazeSquare) super.elementAt (elementCount - index);
    }

    public void print ()
    {
	for (int i = 0; i < elementCount; i ++) {
	    System.out.print ("[");
	    MazeSquare m = (MazeSquare) elementAt (i);
	    m.print ();
	    System.out.print ("] ");
	}
	System.out.println ("");
    }
}
