package local.maze;

import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class MAZEoMatic {
	 public static void main (String args[]) {
	    	final MazeApplet app = new MazeApplet();
	    	app.init();
	    	app.start();
	    	Frame f = new Frame("MAZEoMATIC");
	    	f.addWindowListener (
	    	new WindowAdapter() {
	    	public void windowClosing(WindowEvent e) {
	    	app.stop();
	    	app.destroy();
	    	System.exit(0);
	    	}
	    	}
	    	);
	    	f.setSize (800, 1000);
	    	f.add("Center", app);
	    	f.show();
	    	}
}
