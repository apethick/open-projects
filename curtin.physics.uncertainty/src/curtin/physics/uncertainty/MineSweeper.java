package curtin.physics.uncertainty;

import java.util.Random;
import java.awt.Color;
import java.awt.Font;
import java.lang.String;
import java.awt.event.*;

public class MineSweeper implements MouseListener{

    private int numC;                                      // number of Columns
    private int numR;                                      // number of Rows
    private int numM;                                      // number of Mines
    private SimpleCanvas picLay;                           // new blank SimpleCanvas
    private SimpleCanvas mineCounter;                      // new blank SimpleCanvas
    private int counter=0;                                 // counter of planted mines
    private int [] [] layout = new int [0] [0];            // array of new mines
    private Color red;                                     // colours of fonts and simpleCanvas Lines
    private Color blue;
    private Color darkBlue;
    private Color green;
    private Color darkerGrey;
    private Color grey;
    private Color black;
    private Color white;
    private Color darkGrey;
    private Font  defaultFont;
    private boolean [] [] marked;                         // boolean to check if the square is marked
    private boolean over= false;
    private int gameCounter=0;                            // counter for the number of squares uncovered
    private int mineCount =0;
    private int turns =0;
    public static int clear = 0;
    
    
public MineSweeper (int numCols, int numRows, int numMines) {
    numC = numCols;
    numR = numRows;
    numM = numMines;
    layout = new int [numR] [numC];                          // sets up the array for the minefield
    Random r = new Random();
    mineCount = numMines;
    // starts drawing the layout for minesweeper in simpleCanvas. PicLay is the diagram for the new minesweeper
    
    picLay = new SimpleCanvas("MineSweeper",numCols * 30, numRows*30); // creates the new pictorial field
    mineCounter = new SimpleCanvas("MineSweeper Counter",200, 60);          // creates the new pictorial field
  
  grey = new Color(230,230,230);                                       // colour for the background of the mineSweeper
  darkGrey = new Color(100,100,100);                                   // colour for the dug squares
  black = new Color(0,0,0);                                            // colours for the lines
  picLay.setForegroundColour(grey);                                    // setting up the background colour
  
  for(int i = 0 ; i <numCols*30 ; i++){
    picLay.drawLine (i,0,i,numRows*30);                                // setting up the simpleCanvas of plain grey
    }
    
   picLay.setForegroundColour(black);                                  // sets up the colour of the lines as black
  for(int i = 0 ; i < numCols*30 ; i=i+30) {                           // draws vertical lines 30 pixels per box wide
     picLay.drawLine(i,0,i,numRows*30);                                // to distinguish between columns
    }
  for(int i = 0 ; i < numRows*30; i= i+30){                             // draws the horizontal lines to distinguish between rows
     picLay.drawLine(0,i,numCols*30,i);
    }
    picLay.setForegroundColour(darkGrey);  
    for(int i = 29 ; i < numCols*30 ; i=i+30) {                          // draws vertical lines 30 pixels per box wide
     picLay.drawLine(i,0,i,numRows*30);   
    }
  for(int i = 29 ; i < numRows*30; i= i+30){
     picLay.drawLine(0,i,numCols*30,i);
     }  
    
    for (int i = 0; i<numR ; i ++) {                                   // initializes each block in the array to 0;
     for (int n = 0; n < numC ; n++) {
      layout [i][n] = clear;
    
     }
     mineCounter.setForegroundColour(black);
     mineCounter.drawString ("Mines Left : " + numM,10,30);
    }
    
   
    
  // puts the random mines places.
  
        while(counter < numMines && counter < numRows*numCols) {
            int randomRow = r.nextInt(numR);
            int randomCol = r.nextInt(numC);
            if (layout [randomRow][randomCol] == 0) {
                layout [randomRow][randomCol] = 1;
                counter++;
                }
             else if (layout [randomRow][randomCol]!=0) {
              counter = counter;   
             }
           }
           

  picLay.addMouseListener(this);
  picLay.frame.setVisible(true);

}
  
public static void main(String[] args) {
	MineSweeper s = new MineSweeper(10,10,15);
}


public void printout () {
   for (int i = 0 ; i < layout.length ; i++) {
    for (int n = 0 ; n < layout[i].length; n++) {
     System.out.print(layout[i][n] + " ");   
    }
    System.out.println();
   }


}

public int [][] printO () {

return layout;
}
  

  
public MineSweeper(boolean[][] mineField) {
    layout =  new int [mineField.length][mineField[0].length];
    for (int i = 0 ; i < mineField.length ; i++) {
    for (int n = 0 ; n < mineField[i].length ; n++) {
       if (mineField[i][n] == true) {
           layout[i][n] = 1;
       }
       else if (mineField[i][n] == false) {
           layout[i][n] = 0;}           
    }
        
    }


}


public void mouseClicked(MouseEvent e) {

  int x = e.getX();
  int y = e.getY();
  int z = e.getButton();
  if (z==1) {
  dig (x/30,y/30);}
  else if (z==3) {
   mark(x/30,y/30);   
  }
  // calculate the cell number

}
public void mouseEntered(MouseEvent e) {
}
public void mouseExited(MouseEvent e) {
}
public void mousePressed(MouseEvent e) {
}
public void mouseReleased(MouseEvent e) {
}




public void dig(int i, int j) {
       
   
   int number = 0;
   turns ++;
   defaultFont = new Font ("hello",10,15);
    darkerGrey = new Color (100,100,100);
    white  = new Color (255,255,255);
    red = new Color (255,0,0);
   
   if (over == false) {
   if (layout[j][i] == 3) {}
   if (layout[j][i] == 4) {}
   if (layout[j][i] == 2) {}
   
   else if (layout[j][i]!=2) {
       if (layout[j][i] == 1) {
   
       picLay.setForegroundColour(darkerGrey);
    for (int a = j*30+1 ; a < (j*30+29) ; a++){
        picLay.drawLine(i*30+1,a,i*30+29,a);       
    }
    picLay.setForegroundColour(black);
       picLay.drawLine(i*30+15,j*30+5,i*30+15,j*30+24);
       picLay.drawLine(i*30+14,j*30+5,i*30+14,j*30+24);     // vertical line of mine
       picLay.drawLine(i*30+16,j*30+5,i*30+16,j*30+24);
       
       picLay.drawLine(i*30+25,j*30+15,i*30+5,j*30+15);
       picLay.drawLine(i*30+25,j*30+16,i*30+5,j*30+16);    // horizontal line of mine
       picLay.drawLine(i*30+25,j*30+14,i*30+5,j*30+14);
       
       picLay.drawLine(i*30+5,j*30+5,i*30+23,j*30+25);
       picLay.drawLine(i*30+6,j*30+5,i*30+24,j*30+25);     // down diagonal
       picLay.drawLine(i*30+7,j*30+5,i*30+25,j*30+25);
       
       picLay.drawLine(i*30+25,j*30+5,i*30+7,j*30+25);
       picLay.drawLine(i*30+24,j*30+5,i*30+6,j*30+25);     // up diagonal
       picLay.drawLine(i*30+23,j*30+5,i*30+5,j*30+25);
       
         
     
    
       picLay.setForegroundColour(black);
       picLay.setFont(defaultFont);
       picLay.drawString ("---GAME OVER!---",numC*30/2-60,numR*30/2);
       picLay.drawString ("---GAME OVER!---",numC*30/2-59,numR*30/2);
       picLay.drawString ("---GAME OVER!---",numC*30/2-60,numR*30/2-1);
       picLay.drawString ("---GAME OVER!---",numC*30/2-60,numR*30/2+1);
       
      layout[j][i] = 2;
      over = true;
   }
    else if (layout[j][i] !=1) {
   number = topLeft(i,j) + middleTop(i,j) +topRight(i,j)
            +middleRight(i,j) +bottomRight(i,j) 
            +middleBottom(i,j) +bottomLeft(i,j) 
            +middleLeft(i,j);
            
   String s = "" +number;
        
        
 picLay.setForegroundColour(darkerGrey);
    for (int a = j*30+1 ; a < (j*30+29) ; a++){
        picLay.drawLine(i*30+1,a,i*30+29,a);       
    }
    
     picLay.setForegroundColour(red);
     picLay.setFont(defaultFont);
    picLay.drawString (s,i*30+11,j*30+21);
    gameCounter ++;
    layout[j][i] = 2;
    if (gameCounter == numR*numC - numM) {
        over = true;
        picLay.setForegroundColour(black);
             picLay.setForegroundColour(white);
            picLay.drawString ("YOU WIN!!!! Congratulations",numC*30/2-90,numR*30/2);          
            }
    }
        if (number == 0){
            topLeftA(i,j);
            topRightA(i,j);
            middleTopA(i,j);
            middleRightA(i,j);
            middleLeftA(i,j);
            bottomLeftA(i,j);
            bottomRightA(i,j);
            middleBottomA(i,j);}   
   
      }
    }

        
  
  
  
 }
    
  public void mark(int i, int j) {
      red = new Color (255,0,0);
      black = new Color (0,0,0);
      white = new Color (255,255,255);
      darkerGrey = new Color (100,100,100);
      picLay.setForegroundColour(red);
      grey = new Color(230,230,230);
      
      
      if (layout[j][i] == 0) {
           mineCount --;
          for (int n = i*30+4 ; n < i*30+18 ; n++){
            picLay.drawLine(n,j*30+4,n,j*30+11);
          }
          picLay.setForegroundColour(black);
          for (int n = i*30+18;n<i*30+20 ;n++) {
            picLay.drawLine(n,j*30+4,n,j*30+21);   
          }
          for (int n = j*30+21;n<j*30+23 ;n++) {
            picLay.drawLine(i*30+7,n,i*30+26,n);   
          }
          layout[j][i] = 4;
          
          mineCounter.setForegroundColour(white);
          for (int k = 20 ; k < 40 ; k++) {
           mineCounter.drawLine(70,k,150,k);   
          }
          mineCounter.setForegroundColour(black);
          mineCounter.drawString ("" + (mineCount),70,30);
         
      }
      
      
      
      else if (layout[j][i] == 1) {
          mineCount --;
          for (int n = i*30+4 ; n < i*30+18 ; n++){
            picLay.drawLine(n,j*30+4,n,j*30+11);
          }
          picLay.setForegroundColour(black);
          for (int n = i*30+18;n<i*30+20 ;n++) {
            picLay.drawLine(n,j*30+4,n,j*30+21);   
          }
          for (int n = j*30+21;n<j*30+23 ;n++) {
            picLay.drawLine(i*30+7,n,i*30+26,n);   
          }
          layout[j][i] = 3;
          
           mineCounter.setForegroundColour(white);
          for (int k = 20 ; k < 40 ; k++) {
           mineCounter.drawLine(70,k,150,k);   
          }
          mineCounter.setForegroundColour(black);
          mineCounter.drawString ("" + (mineCount),70,30);
      }
      
      else if (layout[j][i] == 2) {
          
      }
      
      else if (layout[j][i] == 3) {
          mineCount++;
          picLay.setForegroundColour(grey);
            for (int a = j*30+2 ; a < (j*30+28) ; a++){
                picLay.drawLine(i*30+2,a,i*30+28,a);       
                }
         layout[j][i] = 1;
          mineCounter.setForegroundColour(white);
          for (int k = 20 ; k < 40 ; k++) {
           mineCounter.drawLine(70,k,150,k);   
          }
          mineCounter.setForegroundColour(black);
          mineCounter.drawString ("" + (mineCount),70,30);
      }
      
      
      else if (layout[j][i] == 4) {
          mineCount++;
          picLay.setForegroundColour(grey);
            for (int a = j*30+2 ; a < (j*30+28) ; a++){
                picLay.drawLine(i*30+2,a,i*30+28,a);       
                }
         layout[j][i] = 0;
      }
       mineCounter.setForegroundColour(white);
          for (int k = 20 ; k < 40 ; k++) {
           mineCounter.drawLine(70,k,150,k);   
          }
          mineCounter.setForegroundColour(black);
          mineCounter.drawString ("" + (mineCount),70,30);
  }



// ----------------------------------------------------------------------------------------------------------------
private int topLeft(int i, int j){
int counter =0;
    if (i <= 0 || j <= 0) {counter = counter;} 
    else if (j > 0 && i >0) {
        if (layout[j-1][i-1] == 1 || layout[j-1][i-1] == 3) {counter++;}
        else if (layout[j-1][i-1] == 0) {counter = counter;}}
        return counter;
    }
    
private int middleLeft(int i, int j){
int counter = 0;
    if (i <= 0) {counter = counter;} 
    else if (i > 0) {
        if (layout[j][i-1] == 1 || layout[j][i-1] == 3) {counter++;}
        else if (layout[j][i-1] != 1) {counter = counter;}}
        return counter;
    }
    
private int bottomLeft(int i, int j){
    int counter = 0;
    if (j >= numR-1 || i <=0 ) {counter = counter;} 
    else if (j < numR - 1 && i > 0) {
        if (layout[j+1][i-1] == 1 || layout[j+1][i-1] == 3) {counter++;}
        else if (layout[j+1][i-1] != 1) {counter = counter;}}
        return counter;
    }

private int middleBottom(int i, int j){
    int counter = 0;
    if (j >= numR-1) {counter = counter;} 
    else if (j < numR - 1) {
        if (layout[j+1][i] == 1) {counter++;}
        else if (layout[j+1][i] != 1) {counter = counter;}}
        return counter;
    }
    
private int middleTop(int i, int j){
    int counter = 0;
    if (j <= 0) {counter = counter;} 
    else if (j > 0) {
        if (layout[j-1][i] == 1 || layout[j-1][i] == 3) {counter++;}
        else if (layout[j-1][i] != 1) {counter = counter;}}
        return counter;
    }
    
private int bottomRight(int i, int j){
    int counter = 0;
    if (j >= numR-1 || i >= numC - 1 ) {counter = counter;} 
    else if (j < numR - 1 && i < numC - 1) {
        if (layout[j+1][i+1] == 1 || layout[j+1][i+1] == 3) {counter++;}
        else if (layout[j+1][i+1] != 1) {counter = counter;}}
        return counter;
    }
    
private int middleRight(int i, int j){
int counter = 0;
    if (i >= numC-1) {counter = counter;} 
    else if (i < numC -1) {
        if (layout[j][i+1] == 1 || layout[j][i+1] == 3) {counter++;}
        else if (layout[j][i+1] != 1) {counter = counter;}}
        return counter;
    }

private int topRight (int i, int j){
    int counter = 0;
    if (i >= numC-1 || j == 0) {counter = counter;} 
    else if (i < numC - 1 && j > 0) {
        if (layout[j-1][i+1] == 1 || layout[j-1][i+1] == 3) {counter++;}
        else if (layout[j-1][i+1] != 1) {counter = counter;}}
        return counter;
    }

public void printMineOutput () {
System.out.println("");
System.out.println("minesweeper layout");
printout();
System.out.println("--------------top Right------------");
for (int i = 0 ; i < numR ; i++) {
   for (int n = 0 ; n < numC ;n++) {
       System.out.print(topRight(n,i) + " ");}
       System.out.println();
}
System.out.println("--------------middle Right------------");
for (int i = 0 ; i < numR ; i++) {
   for (int n = 0 ; n < numC;n++) {
       System.out.print(middleRight(n,i) + " ");}
       System.out.println();
}
System.out.println("--------------bottomRight------------");
for (int i = 0 ; i < numR ; i++) {
   for (int n = 0 ; n < numC ;n++) {
       System.out.print(bottomRight(n,i) + " ");}
       System.out.println();
}
System.out.println("--------------middleBottom------------");
for (int i = 0 ; i < numR ; i++) {
   for (int n = 0 ; n < numC ;n++) {
       System.out.print(middleBottom(n,i) + " ");}
       System.out.println();
}
System.out.println("--------------bottomLeft------------");
for (int i = 0 ; i < numR ; i++) {
   for (int n = 0 ; n < numC;n++) {
       System.out.print(bottomLeft(n,i) + " ");}
       System.out.println();
}
System.out.println("--------------middle Left------------");
for (int i = 0 ; i < numR ; i++) {
   for (int n = 0 ; n < numC ;n++) {
       System.out.print(middleLeft(n,i) + " ");}
       System.out.println();
}
System.out.println("--------------top Left------------");
for (int i = 0 ; i < numR ; i++) {
   for (int n = 0 ; n < numC ;n++) {
       System.out.print(topLeft(n,i) + " ");}
       System.out.println();
}
System.out.println("--------------top centre------------");
for (int i = 0 ; i < numR; i++) {
   for (int n = 0 ; n < numC;n++) {
       System.out.print(middleTop(n,i) + " ");}
       System.out.println();
}
System.out.println();
}


// ---------------- AUTO REVEAL SECTION ----------------------------\\    
private void topLeftA(int i, int j){

    if (i <= 0 || j <= 0) {} 
    else if (j > 0 && i >0) {
        dig(i-1 , j-1);
    }
}
    
private void middleLeftA(int i, int j){
    if (i <= 0) {} 
    else if (i > 0) {
        dig(i-1,j);
    }
}
    
private void bottomLeftA(int i, int j){
    if (j >= numR-1 || i <=0 ) {} 
    else if (j < numR - 1 && i > 0) {
        dig(i -1 , j +1);
    }
}

private void middleBottomA(int i, int j){

    if (j >= numR-1) {} 
    else if (j < numR - 1) {
        dig(i,j+1);
    }
}
    
private void middleTopA(int i, int j){
    int counter = 0;
    if (j <= 0) {} 
    else if (j > 0) {
        dig(i,j-1);
        }
       
    }
    
private void bottomRightA(int i, int j){
  
    if (j >= numR-1 || i >= numC - 1 ) {} 
    else if (j < numR - 1 && i < numC - 1) {
        dig(i+1,j+1);
        }
        
    }
    
private void middleRightA(int i, int j){
    if (i >= numC-1) {} 
    else if (i < numC -1) {
        dig(i+1,j);
        }
        
    }

private void topRightA (int i, int j){

    if (i >= numC-1 || j == 0  ) {} 
    else if (i < numC - 1 && j > 0) {
        dig(i+1,j-1);
        }
       
    }

private int numberMines (int i, int j) {
int number=0;
number = topLeft(i,j) + middleTop(i,j) +topRight(i,j)
            +middleRight(i,j) +bottomRight(i,j) 
            +middleBottom(i,j) +bottomLeft(i,j) 
            +middleLeft(i,j);
return number;
}


}