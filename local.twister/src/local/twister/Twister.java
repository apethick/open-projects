package local.twister;
import java.applet.*;
	import javax.swing.*;
	import java.io.*;
import java.net.*;
	
public class Twister  extends JFrame{
	private AudioClip red, green, blue, yellow, right_hand, left_hand, right_leg, left_leg, title; // Sound player
	URL titleURL= this.getClass().getResource("title.wav");	  
	URL redURL = this.getClass().getResource("red.wav");	  
    URL blueURL = this.getClass().getResource("blue.wav"); // Get the Sound URL
    URL greenURL = this.getClass().getResource("green.wav");	  
    URL yellowURL = this.getClass().getResource("yellow.wav"); // Get the Sound URL
    URL right_handURL = this.getClass().getResource("right_hand.wav");	  
    URL left_handURL = this.getClass().getResource("left_hand.wav"); // Get the Sound URL
    URL right_legURL = this.getClass().getResource("right_leg.wav");	  
    URL left_legURL = this.getClass().getResource("left_leg.wav"); // Get the Sound URL

    int counter []= {0,0,0,0};
    	    
	 
	  public Twister (int wait)  {
		  
	      try     {
	    	  title = Applet.newAudioClip(titleURL);  
	    	  red = Applet.newAudioClip(redURL);
	    	    green = Applet.newAudioClip(blueURL);
	    	    blue = Applet.newAudioClip(greenURL);
	    	    yellow = Applet.newAudioClip(yellowURL);
	    	    right_hand = Applet.newAudioClip(right_handURL);
	    	    left_hand = Applet.newAudioClip(left_handURL);
	    	    right_leg = Applet.newAudioClip(right_legURL);
	    	    left_leg = Applet.newAudioClip(left_legURL);
	    try {
	    	title.play();
	    	Thread.sleep(8000);
	    	
            while(counter[0] < 5000) {
            	playRandom();
            	Thread.sleep(wait);
            }
            
        }
        catch (Exception f) {
            f.printStackTrace();
        }
	
	      }  catch(Exception e){
	    	  e.printStackTrace();
	    	  
	      } 
	  }
	  
	  
	  
	  public void playRandom() {
		  int colour = (int) (Math.random()*4);
          int limb = (int) (Math.random()*4);
          
          switch (limb) {
    		case 0:
    			left_hand.play();
    			break;
    		case 1:
    			right_hand.play();
    			break;
    		case 2:
    			left_leg.play();
    			break;
    		case 3:
    			right_leg.play();
    			break;
    		default:
    			break;
    		}
          
          
          try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		switch (colour) {
		case 0:
			red.play();
		
			break;
		case 1:
			blue.play();
			break;
		case 2:
			green.play();
			break;
		case 3:
			yellow.play();
			break;
		default:
			break;
		}
          
	  }

	
	public static void main(String[] args) {
		Twister t = new Twister(Integer.valueOf(12000));
	}
}
