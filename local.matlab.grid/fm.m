function solution = fm(m,t)
     A = m(1);
     B = m(2);
     w1 = m(3);
     w2 = m(4);     
	 solution = A*sin(w1*t)+ B*cos(w2*t);