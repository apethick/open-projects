package local.matlab.grid;
import java.awt.Color;


public class Matlab1D {
	public String title;
	public String xSeriesName;
	public String ySeriesName;
	public String xLabel;
	public String yLabel;
	public String seriesName;
	public Color lineColour;
	public int lineSize;
	public String style;
	
	public double [] x;
	public double [] y;	
	
	public Matlab1D (double [] x, double [] y) {
		this.x = x;
		this.y = y;
		this.title = "";
		this.xLabel = "x";
		this.yLabel = "y";
		this.seriesName = "Series";	
		this.xSeriesName = "x";
		this.ySeriesName = "y";
		this.lineColour = Color.blue;
		this.lineSize = 2;
	}
	
	public Matlab1D(double [] x, double [] y, String title, String xSeriesName, String ySeriesName, String xLabel, String yLabel, String seriesName) {
		this.x = x;
		this.y = y;
		this.title = title;
		this.xLabel = xLabel;
		this.yLabel = yLabel;
		this.seriesName = seriesName;		
		this.xSeriesName = xSeriesName;
		this.ySeriesName = ySeriesName;
		this.lineColour = Color.blue;
		this.lineSize = 2;
	}
}
