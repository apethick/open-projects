package local.matlab.grid;
import java.awt.Color;
import java.util.ArrayList;


public class Matlab1DGridFunctions {

	/*plots a single profiles on the one graph*/
	public static ArrayList <String> plotSingleGraph(Matlab1D plot, int figureNumber, int width, int height, boolean xLog, boolean yLog) {
		ArrayList<String> commands = new ArrayList<String>();
		String s = "";
		commands.add(new String("fig"+figureNumber + "=figure(" + figureNumber + ");"));
		commands.add("set(fig" + figureNumber + " , 'Color',[1,1,1],'Position',[0,0," + width + "," + height + "]);");
		commands.add((plot.xSeriesName + " = " + arrayToString(plot.x)));
		commands.add((plot.ySeriesName + " = " + arrayToString(plot.y)));
		
		commands.add( (xLog ? yLog? "loglog" : "semilogx" : yLog ? "semilogy" : "plot") + "(" + plot.xSeriesName + "," + plot.ySeriesName + ",'color',[" + plot.lineColour.getRed()/255 + " " + plot.lineColour.getGreen()/255 + " " + plot.lineColour.getBlue()/255 + "],'LineWidth'," + plot.lineSize + ")");
		commands.add(new String("hold on"));
		if(xLog || yLog) commands.add(new String("grid"));
		commands.add(("title(['" + plot.title + "'],'FontName','Times','FontWeight','bold','FontSize', 12);"));
		commands.add(("xlabel(['" + plot.xLabel + "'],'FontName','Times','FontWeight','bold','FontSize', 12);"));
		commands.add(("ylabel(['" + plot.yLabel + "'],'FontName','Times','FontWeight','bold','FontSize', 12);"));
		commands.add(new String("hold off"));	
		return commands;
	}
	
	/*plots multiple profiles on the separate graphs*/
	public static ArrayList <String> plotMultiGraph(ArrayList<Matlab1D> plots, int startFigureNumber, int width, int height, boolean xLog, boolean yLog) {
		ArrayList<String> commands = new ArrayList<String>();
		String s = "";
		int figureNumber = startFigureNumber;
		for(Matlab1D plot : plots) {
			commands.add(new String("fig"+figureNumber + "=figure(" + figureNumber + ");"));
			commands.add("set(fig" + figureNumber + " , 'Color',[1,1,1],'Position',[0,0," + width + "," + height + "]);");
			commands.add((plot.xSeriesName + " = " + arrayToString(plot.x)));
			commands.add((plot.ySeriesName + " = " + arrayToString(plot.y)));
			
			commands.add( (xLog ? yLog? "loglog" : "semilogx" : yLog ? "semilogy" : "plot") + "(" + plot.xSeriesName + "," + plot.ySeriesName + ",'color',[" + plot.lineColour.getRed()/255 + " " + plot.lineColour.getGreen()/255 + " " + plot.lineColour.getBlue()/255 + "],'LineWidth'," + plot.lineSize + ")");
			commands.add(new String("hold on"));
			
			if(xLog || yLog) commands.add(new String("grid"));
			
			commands.add(("title(['" + plot.title + "'],'FontName','Times','FontWeight','bold','FontSize', 12);"));
			commands.add(("xlabel(['" + plot.xLabel + "'],'FontName','Times','FontWeight','bold','FontSize', 12);"));
			commands.add(("ylabel(['" + plot.yLabel + "'],'FontName','Times','FontWeight','bold','FontSize', 12);"));
			commands.add(new String("hold off"));	
			figureNumber++;
		}
		
		return commands;
	}
	
	/*plots multiple profiles on the one graph*/
	public static ArrayList <String> plotMultiOnSameGraph(ArrayList<Matlab1D> plots, int figureNumber, int width, int height, boolean xLog, boolean yLog) {
		ArrayList<String> commands = new ArrayList<String>();
		String s = "";
		commands.add(new String("fig"+figureNumber + "=figure(" + figureNumber + ");"));
		commands.add("set(fig" + figureNumber + " , 'Color',[1,1,1],'Position',[0,0," + width + "," + height + "]);");
		
		Matlab1D plotFirst = plots.get(0);
		
		commands.add((plotFirst.xSeriesName + " = " + arrayToString(plotFirst.x)));
		commands.add((plotFirst.ySeriesName + " = " + arrayToString(plotFirst.y)));		
		commands.add( (xLog ? yLog? "loglog" : "semilogx" : yLog ? "semilogy" : "plot") + "(" + plotFirst.xSeriesName + "," + plotFirst.ySeriesName + ",'color',[" + plotFirst.lineColour.getRed()/255 + " " + plotFirst.lineColour.getGreen()/255 + " " + plotFirst.lineColour.getBlue()/255 + "],'LineWidth'," + plotFirst.lineSize + ")");
		if(xLog || yLog) commands.add(new String("grid"));
		commands.add(new String("hold on"));
		int i = 0;
		for(Matlab1D plot : plots) {
			if(i != 0) {
				commands.add((plot.xSeriesName + " = " + arrayToString(plot.x)));
				commands.add((plot.ySeriesName + " = " + arrayToString(plot.y)));			
				commands.add( (xLog ? yLog? "loglog" : "semilogx" : yLog ? "semilogy" : "plot") + "(" + plot.xSeriesName + "," + plot.ySeriesName + ",'color',[" + plot.lineColour.getRed()/255 + " " + plot.lineColour.getGreen()/255 + " " + plot.lineColour.getBlue()/255 + "],'LineWidth'," + plot.lineSize + ")");													
			}
			i++;
		}
		commands.add(("title(['" + plotFirst.title + "'],'FontName','Times','FontWeight','bold','FontSize', 12);"));
		commands.add(("xlabel(['" + plotFirst.xLabel + "'],'FontName','Times','FontWeight','bold','FontSize', 12);"));
		commands.add(("ylabel(['" + plotFirst.yLabel + "'],'FontName','Times','FontWeight','bold','FontSize', 12);"));
		commands.add(new String("hold off"));	
		return commands;
	}
	
	private static String arrayToString(double [] array) {
		String s = "[";
		for (int i = 0 ; i < array.length ; i++) {
			s += ("" + array[i]);
			if(i != (array.length -1)) {
				s += ",";
			}
		}
		s += "];";
		return s;
	}
	public static void main(String[] args) {
//		executeMFileWindows("C:/Program Files/MATLAB/R2008b/bin/matlab ", "NonLinear");
		double [] x = {0,1,2,3,4};
		double [] y = {0,1,4,9,16};
		double [] y2 = {0,1,8,27,64};
		
		Matlab1D plot = new Matlab1D(x,y);
			plot.title = "Test Plot";
			plot.ySeriesName = "Amplitude";
			plot.xSeriesName = "Offset";
			plot.xLabel = "Offset";
			plot.yLabel = "Amplitude";
			
			Matlab1D plot2 = new Matlab1D(x,y2);
			plot2.title = "Test Plot";
			plot2.ySeriesName = "Amplitude2";
			plot2.xSeriesName = "Offset";
			plot2.xLabel = "Offset";
			plot2.yLabel = "Amplitude";
			plot2.lineColour = Color.GREEN;
		ArrayList<String> commands = plotSingleGraph(plot, 1, 800, 450,false, true);
		
		ArrayList<Matlab1D> plots = new ArrayList<Matlab1D>();
		plots.add(plot);
		plots.add(plot2);
		
		ArrayList<String> commands2 = plotMultiGraph(plots, 1, 800, 450,false, true);
		
		for (String s : commands2) {
			System.out.println(s);
		}
	}
}
