 function derivatives = derivs(m,t)
     A = m(1);
     B = m(2);
     w1 = m(3);
     w2 = m(4); 
derivatives = [sin(w1*t);cos(w2*t);A*t.*cos(w1*t);-B*t.*sin(w2*t)]'; %A