clc;
clear all
load noise02;
d = noise;
t = 0.001 : 0.001 : 1;
f = 50;
w = 2*pi*f;

%LINEAR INVERSION
G = [sin(t*w) ; cos(t*w)]';
m = (G'*G)^-1*(G'*d')
m_lin = m;
%GAUSS NEWTON NON-LINEAR INVERSION
%Initialising parameters
A = m(1);
B = m(2);
w1 = 2*pi*50;
w2 = 2*pi*50;
m = [A,B,w1,w2];


for k = 1 : 1 : 4
    fwm = fm(m,t);  %initial starting model
    y = (d - fwm)';          %Difference vector
    C = derivs(m,t);      %A  Partial Derivatives
    nx = (C'*C)^-1*C'*y;          %Computing next steps      
    m = m + nx';
    q(k) = (d-fwm)*(d-fwm)';  %Least squares calculation
        figure(k)    
        
        hold on
        h = legend('Raw','Residual','Modelled',2);
        title(['Non-Linear Inversion Iteration - Iteration ',num2str(k),' q = ',num2str(q(k))]);
        xlabel(['Time(s)']);
        ylabel(['Amplitude']);
        plot(t,d-fwm,'g','linewidth',2);

        hold off
        print -f1 -dmeta image
end

'hello world'

% figure(100)
% hold on
% set(gca,'XTickLabel',{'1','','2','','3','','4'})
% title(['Least Squares Error versus Non-Linear Inversion Iteration']);
% xlabel(['Iteration']);
% ylabel(['Least Squares Error']);
% plot(q,'--s','MarkerSize',10)
% hold off

%STEEPEST DESCENT NON-LINEAR INVERSION
% A = m_lin(1);
% B = m_lin(2);
% w1 = 2*pi*50;
% w2 = 2*pi*50;
% m = [A,B,w1,w2];
% 
% 
% 
% k = 10^-4;
% for l = 1 : 1 : 300
%     fwm = fm(m,t);  %initial starting model
%     y = (d - fwm)';          %Difference vector
%     C = derivs(m,t);      %A  Partial Derivatives
%     x = k.*C'*y;
%     m = m + x';          %Computing next steps      
%     q4(l) = (d-fwm)*(d-fwm)';  %Least squares calculation
% %         figure(k)        
% %         hold on
% %         h = legend('Raw','Residual','Modelled',2);
% %         title(['Non-Linear Inversion Iteration - Iteration ',num2str(k),' q = ',num2str(q(k))]);
% %         xlabel(['Time(s)']);
% %         ylabel(['Amplitude']);
% %         plot(t,d);
% %         plot(t,d-fwm,'g');
% %         plot(t,fwm,'r');
% %         hold off
% end
% 
% A = m_lin(1);
% B = m_lin(2);
% w1 = 2*pi*50;
% w2 = 2*pi*50;
% m = [A,B,w1,w2];
% k = 10^-5;
% for l = 1 : 1 : 300
%     fwm = fm(m,t);  %initial starting model
%     y = (d - fwm)';          %Difference vector
%     C = derivs(m,t);      %A  Partial Derivatives
%     x = k.*C'*y;
%     m = m + x' ;         %Computing next steps      
%     q5(l) = (d-fwm)*(d-fwm)';  %Least squares calculation
% end
% 
% A = m_lin(1);
% B = m_lin(2);
% w1 = 2*pi*50;
% w2 = 2*pi*50;
% m = [A,B,w1,w2];
% k = 10^-6;
% for l = 1 : 1 : 300
%     fwm = fm(m,t);  %initial starting model
%     y = (d - fwm)';          %Difference vector
%     C = derivs(m,t);      %A  Partial Derivatives
%     x = k.*C'*y;
%     m = m + x' ;         %Computing next steps      
%     q6(l) = (d-fwm)*(d-fwm)';  %Least squares calculation
% end
% 
% A = m_lin(1);
% B = m_lin(2);
% w1 = 2*pi*50;
% w2 = 2*pi*50;
% m = [A,B,w1,w2];
% k = 10^-3;
% for l = 1 : 1 : 300
%     fwm = fm(m,t);  %initial starting model
%     y = (d - fwm)';          %Difference vector
%     C = derivs(m,t);      %A  Partial Derivatives
%     x = k.*C'*y;
%     m = m + x' ;         %Computing next steps      
%     q3(l) = (d-fwm)*(d-fwm)';  %Least squares calculation
% end
% 
% 
% figure(100)
% hold on
% title(['Steepest Decent Non-Linear Inversion versus Iterations']);
% xlabel(['Iteration']);
% ylabel(['Least Squares Error']);
% plot(q3,'r','linewidth',2)
% plot(q4,'g','linewidth',2)
% plot(q5,'b','linewidth',2)
% plot(q6,'k','linewidth',2)
% h = legend('k=1E-3','k=1E-4','k=1E-5','k=1E-6',3);
% hold off
